package com.epam.lab.mobile_network;

import com.epam.lab.mobile_network.controllers.Controller;

public class App {

    public static void main(String[] args) {
        new Controller().showMenu();
    }

}
