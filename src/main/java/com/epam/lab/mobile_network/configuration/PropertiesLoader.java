package com.epam.lab.mobile_network.configuration;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertiesLoader {
    private static Logger logger = LogManager.getLogger("ConsoleLogger");
    private static final String PROPERTIES_FILE_NAME = "network.properties";

    public static Properties loadProperties() {
        Properties configuration = new Properties();

        try (InputStream inputStream = PropertiesLoader.class
                .getClassLoader()
                .getResourceAsStream(PROPERTIES_FILE_NAME)) {
            configuration.load(inputStream);
        } catch (IOException ex) {
            logger.error("Cannot load application file {}: {}", PROPERTIES_FILE_NAME, ex.getMessage());
        }

        return configuration;
    }
}