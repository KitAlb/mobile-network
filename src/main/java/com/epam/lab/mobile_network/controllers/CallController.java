package com.epam.lab.mobile_network.controllers;

import com.epam.lab.mobile_network.services.CallService;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Collection;
import java.util.Scanner;

class CallController {
    private static Logger logger = LogManager.getLogger("ConsoleLogger");
    private static Scanner input = new Scanner(System.in);
    private static CallService callService = new CallService();

    static void findAllCalls() {
        callService.findAll().ifPresentOrElse(
                CallController::printCollection,
                CallController::printReadError
        );
    }

    static void findCallsForUser() {
        logger.info("Please specify user UUID:");
        String userId = input.next();
        callService.findBySenderId(userId).ifPresentOrElse(
                CallController::printCollection,
                CallController::printReadError
        );
    }

    static void doCall() {
        logger.info("Please specify caller UUID:");
        String senderId = input.next();
        logger.info("Please specify receiver UUID:");
        String receiverId = input.next();
        int count = callService.doCall(senderId, receiverId);
        logger.info("Created {} records", count);
    }

    private static void printCollection(Collection<?> collection) {
        if (CollectionUtils.isEmpty(collection)) {
            logger.info("No calls found");
        } else {
            logger.info("Calls:");
            collection.forEach(item -> logger.info(item));
        }
    }

    private static void printReadError() {
        logger.error("Cannot find data in database");
    }

}
