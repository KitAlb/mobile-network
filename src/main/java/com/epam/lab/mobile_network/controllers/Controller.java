package com.epam.lab.mobile_network.controllers;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class Controller {
    private static Logger logger = LogManager.getLogger("ConsoleLogger");
    private static final int TOP_LEVEL_MENU = 1;
    private static final String SELECT_MENU_MESSAGE = "Please, select menu point";
    private static final String EXIT_KEY = "Q";

    private Map<String, String> menu;
    private Map<String, ControllerMethod> methodsMenu;
    private static Scanner input = new Scanner(System.in);

    public Controller() {
        menu = new LinkedHashMap<>();

        menu.put("1", "1 - Tariff operations");
        menu.put("11", "11 - Find all tariffs");
        menu.put("12", "12 - Find tariffs by name");
        menu.put("13", "13 - Find tariffs by type");
        menu.put("14", "14 - Find tariff by UUID");
        menu.put("15", "15 - Create new tariff");
        menu.put("16", "16 - Update tariff");
        menu.put("17", "17 - Delete tariff");

        menu.put("2", "2 - Notification operations");
        menu.put("21", "21 - Find all notifications");
        menu.put("22", "22 - Find notifications to send by now");
        menu.put("23", "23 - Find notifications for user");
        menu.put("24", "24 - Create notification for user");
        menu.put("25", "25 - Create notification for all users");
        menu.put("26", "26 - Update notification");
        menu.put("27", "27 - Delete notification");

        menu.put("3", "3 - User search operations");
        menu.put("31", "31 - Find all users");
        menu.put("32", "32 - Find user by UUID");
        menu.put("33", "33 - Find user by First Name");
        menu.put("34", "34 - Find user by Last Name");
        menu.put("35", "35 - Find user by Full Name");
        menu.put("36", "36 - Find by Phone Number");
        menu.put("37", "37 - Find by Tariff Id");
        menu.put("38", "38 - Find by Tariff Name");

        menu.put("4", "4 - User creation operations");
        menu.put("41", "41 - Create new user");
        menu.put("42", "42 - Update user name");
        menu.put("43", "43 - Update user phone number");
        menu.put("44", "44 - Change user tariff");
        menu.put("45", "45 - Delete user");
        menu.put("46", "46 - Add money to account");

        menu.put("5", "5 - Calls operations");
        menu.put("51", "51 - Find all calls");
        menu.put("52", "52 - Find all calls for user");
        menu.put("53", "53 - Do call");

        menu.put("6", "6 - Sms operations");
        menu.put("61", "61 - Find all sms");
        menu.put("62", "62 - Find all sms for user");
        menu.put("63", "63 - Find all sms for users");
        menu.put("64", "64 - Send sms");

        menu.put(EXIT_KEY, EXIT_KEY + " - Exit");

        methodsMenu = new LinkedHashMap<>();

        methodsMenu.put("11", TariffController::findAllTariffs);
        methodsMenu.put("12", TariffController::findTariffsByName);
        methodsMenu.put("13", TariffController::findTariffsByType);
        methodsMenu.put("14", TariffController::findTariffById);
        methodsMenu.put("15", TariffController::createTariff);
        methodsMenu.put("16", TariffController::updateTariff);
        methodsMenu.put("17", TariffController::deleteTariff);

        methodsMenu.put("21", NotificationController::findAllNotifications);
        methodsMenu.put("22", NotificationController::findNotificationsToSend);
        methodsMenu.put("23", NotificationController::findNotificationsForUser);
        methodsMenu.put("24", NotificationController::createNotificationForUser);
        methodsMenu.put("25", NotificationController::createNotificationForAll);
        methodsMenu.put("26", NotificationController::updateNotification);
        methodsMenu.put("27", NotificationController::deleteNotification);

        methodsMenu.put("31", UserController::findAllUsers);
        methodsMenu.put("32", UserController::findUserById);
        methodsMenu.put("33", UserController::findUserByFirstName);
        methodsMenu.put("34", UserController::findUserByLastName);
        methodsMenu.put("35", UserController::findUserByFullName);
        methodsMenu.put("36", UserController::findUserByPhoneNumber);
        methodsMenu.put("37", UserController::findUserByTariffId);
        methodsMenu.put("38", UserController::findUserByTariffName);

        methodsMenu.put("41", UserController::createNewUser);
        methodsMenu.put("42", UserController::updateUserInfo);
        methodsMenu.put("43", UserController::updateUserPhoneNumber);
        methodsMenu.put("44", UserController::updateUserTariffId);
        methodsMenu.put("45", UserController::deleteUser);
        methodsMenu.put("46", UserController::addMoneyToAccount);

        methodsMenu.put("51", CallController::findAllCalls);
        methodsMenu.put("52", CallController::findCallsForUser);
        methodsMenu.put("53", CallController::doCall);

        methodsMenu.put("61", SmsController::findAllSms);
        methodsMenu.put("62", SmsController::findSmsForUser);
        methodsMenu.put("63", SmsController::findSmsForUsers);
        methodsMenu.put("64", SmsController::sendSms);

        methodsMenu.put(EXIT_KEY, this::exit);
    }

    private void outputMenu() {
        logger.info("MENU:");
        for (String key : menu.keySet()) {
            if (key.length() == TOP_LEVEL_MENU) {
                logger.info(menu.get(key));
            }
        }
    }

    private void outputSubMenu(String fig) {
        logger.info("SubMENU:");
        for (String key : menu.keySet()) {
            if (key.length() != TOP_LEVEL_MENU &&
                    key.substring(0, 1).equals(fig)) {
                logger.info(menu.get(key));
            }
        }
    }

    public void showMenu() {
        String keyMenu;
        do {
            outputMenu();
            logger.info(SELECT_MENU_MESSAGE);
            keyMenu = input.nextLine().toUpperCase();

            if (keyMenu.matches("^\\d")) {
                outputSubMenu(keyMenu);
                logger.info(SELECT_MENU_MESSAGE);
                keyMenu = input.nextLine().toUpperCase();
            }

            if (methodsMenu.containsKey(keyMenu)) {
                try {
                    methodsMenu.get(keyMenu).run();
                } catch (Exception e) {
                    logger.error("Exception: {}", e.getMessage());
                }
            } else {
                logger.error("Invalid menu number");
            }
        } while (!keyMenu.equals(EXIT_KEY));
    }

    private void exit() {
        logger.info("Exiting...");
    }

}
