package com.epam.lab.mobile_network.controllers;

import com.epam.lab.mobile_network.services.NotificationService;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.time.LocalDateTime;
import java.util.*;

/**
 * Notifications intended to be used with some scheduling service which will periodically check notifications,
 * send sms to users and update or delete sent notification
 */
public class NotificationController {
    private static Logger logger = LogManager.getLogger("ConsoleLogger");
    private static Scanner input = new Scanner(System.in);
    private static NotificationService notificationService = new NotificationService();

    static void findAllNotifications() {
        notificationService.findAll().ifPresentOrElse(
                NotificationController::printCollection,
                NotificationController::printReadError
        );
    }

    static void findNotificationsToSend() {
        notificationService.findUpToDate(LocalDateTime.now()).ifPresentOrElse(
                NotificationController::printCollection,
                NotificationController::printReadError
        );
    }

    static void findNotificationsForUser() {
        logger.info("Please specify user UUID:");
        try {
            UUID id = UUID.fromString(input.next());
            notificationService.findByUserId(id).ifPresentOrElse(
                    NotificationController::printCollection,
                    NotificationController::printReadError
            );
        } catch (Exception ex) {
            logger.error("Exception: {}", ex.getMessage());
        }
    }

    static void createNotificationForUser() {
        int count = notificationService.create(getParams(notificationService.getFields()));
        logger.info("Created {} records", count);
    }

    static void createNotificationForAll() {
        int count = notificationService.createForAll(getParams(notificationService.getCommonFields()));
        logger.info("Created {} records", count);
    }

    static void updateNotification() {
        logger.info("Please specify notification UUID:");
        String id = input.next();
        int count = notificationService.update(id, getParams(notificationService.getFields()));
        logger.info("Updated {} records", count);
    }

    static void deleteNotification() {
        logger.info("Please specify notification UUID:");
        String id = input.next();
        int count = notificationService.delete(id);
        logger.info("Deleted {} records", count);
    }

    private static Map<String, String> getParams(List<String> fields) {
        logger.info("Please specify each parameter for notification.");
        Map<String, String> params = new HashMap<>();
        fields.forEach(field -> {
            logger.info("Specify {}", StringUtils.replace(field, "_", StringUtils.SPACE));
            params.put(field, StringUtils.trim(input.next()));
        });
        return params;
    }

    private static void printCollection(Collection<?> collection) {
        if (CollectionUtils.isEmpty(collection)) {
            logger.info("No notifications found");
        } else {
            logger.info("Notifications:");
            collection.forEach(item -> logger.info(item));
        }
    }

    private static void printReadError() {
        logger.error("Cannot read data from database");
    }
}
