package com.epam.lab.mobile_network.controllers;

import com.epam.lab.mobile_network.services.SmsService;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Collection;
import java.util.Scanner;

class SmsController {
    private static Logger logger = LogManager.getLogger("ConsoleLogger");
    private static Scanner input = new Scanner(System.in);
    private static SmsService smsService = new SmsService();

    static void findAllSms() {
        smsService.findAll().ifPresentOrElse(
                SmsController::printCollection,
                SmsController::printReadError
        );
    }

    static void findSmsForUser() {
        logger.info("Please specify user UUID:");
        String userId = input.next();
        smsService.findBySenderId(userId).ifPresentOrElse(
                SmsController::printCollection,
                SmsController::printReadError
        );
    }

    static void findSmsForUsers() {
        logger.info("Please specify sender UUID:");
        String senderId = input.next();
        logger.info("Please specify receiver UUID:");
        String receiverId = input.next();

        smsService.findBySenderAndReceiverId(senderId, receiverId).ifPresentOrElse(
                SmsController::printCollection,
                SmsController::printReadError
        );
    }

    static void sendSms() {
        logger.info("Please specify sender UUID:");
        String senderId = input.next();
        input.nextLine();
        logger.info("Please specify receiver UUID:");
        String receiverId = input.next();
        input.nextLine();
        logger.info("Please specify sms text:");
        String text = input.nextLine();

        int count = smsService.sendSms(senderId, receiverId, text);
        logger.info("Created {} records", count);
    }

    private static void printCollection(Collection<?> collection) {
        if (CollectionUtils.isEmpty(collection)) {
            logger.info("No sms found");
        } else {
            logger.info("Sms:");
            collection.forEach(item -> logger.info(item));
        }
    }

    private static void printReadError() {
        logger.error("Cannot find data in database");
    }
}
