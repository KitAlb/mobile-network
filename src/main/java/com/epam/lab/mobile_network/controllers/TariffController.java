package com.epam.lab.mobile_network.controllers;

import com.epam.lab.mobile_network.model.type.TariffType;
import com.epam.lab.mobile_network.services.TariffService;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.EnumUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;

class TariffController {
    private static Logger logger = LogManager.getLogger("ConsoleLogger");

    private static TariffService tariffService = new TariffService();
    private static Scanner input = new Scanner(System.in);

    static void findAllTariffs() {
        tariffService.findAll().ifPresentOrElse(
                TariffController::printCollection,
                TariffController::printReadError
        );
    }

    static void findTariffsByName() {
        logger.info("Please specify tariff name:");
        String name = input.next();
        if (StringUtils.isBlank(name)) {
            logger.error("Tariff name cannot be empty");
            return;
        }

        tariffService.findByName(name).ifPresentOrElse(
                TariffController::printCollection,
                TariffController::printReadError
        );
    }

    static void findTariffsByType() {
        logger.info("Please specify tariff type:");
        logger.info("Supported types are: {}", Arrays.toString(TariffType.values()));
        String typeString = input.next();
        if (!EnumUtils.isValidEnum(TariffType.class, typeString)) {
            logger.error("Specified type is not valid");
            return;
        }

        TariffType type = TariffType.valueOf(input.next());
        tariffService.findByType(type).ifPresentOrElse(
                TariffController::printCollection,
                TariffController::printReadError
        );
    }

    static void findTariffById() {
        logger.info("Please specify tariff UUID:");
        try {
            UUID id = UUID.fromString(input.next());
            tariffService.findById(id).ifPresentOrElse(
                    tariff -> logger.info(tariff),
                    TariffController::printReadError
            );
        } catch (Exception ex) {
            logger.error("Exception: {}", ex.getMessage());
        }
    }

    static void createTariff() {
        int count = tariffService.create(getParams());
        logger.info("Created {} records", count);
    }

    static void updateTariff() {
        logger.info("Please specify tariff UUID:");
        String id = input.next();
        int count = tariffService.update(id, getParams());
        logger.info("Updated {} records", count);
    }

    static void deleteTariff() {
        logger.info("Please specify tariff UUID:");
        String id = input.next();
        int count = tariffService.delete(id);
        logger.info("Deleted {} records", count);
    }

    private static Map<String, String> getParams() {
        logger.info("Please specify each parameter for tariff.");
        logger.info("Supported tariff types are: {}", Arrays.toString(TariffType.values()));
        Map<String, String> params = new HashMap<>();
        tariffService.getFields().forEach(field -> {
            logger.info("Specify {}", StringUtils.replace(field, "_", StringUtils.SPACE));
            params.put(field, StringUtils.trim(input.next()));
        });
        return params;
    }

    private static void printCollection(Collection<?> collection) {
        if (CollectionUtils.isEmpty(collection)) {
            logger.info("No tariffs found");
        } else {
            logger.info("Tariffs:");
            collection.forEach(item -> logger.info(item));
        }
    }

    private static void printReadError() {
        logger.info("Cannot find data in database");
    }
}
