package com.epam.lab.mobile_network.controllers;

import com.epam.lab.mobile_network.services.UserService;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;

class UserController {
    private static Logger logger = LogManager.getLogger("ConsoleLogger");
    private static Scanner input = new Scanner(System.in);
    private static UserService userService = new UserService();

    static void findAllUsers() {
        userService.findAll().ifPresentOrElse(
                UserController::printCollection,
                UserController::printReadError
        );
    }

    static void findUserById() {
        logger.info("Please specify user UUID:");
        String id = input.next();
        userService.findById(id).ifPresentOrElse(
                user -> logger.info(user),
                () -> logger.error("No user with UUID '{}' in database", id)
        );
    }

    static void findUserByFirstName() {
        logger.info("Please specify user First Name:");
        String name = input.next();
        userService.findByFirstName(name).ifPresentOrElse(
                UserController::printCollection,
                UserController::printReadError
        );
    }

    static void findUserByLastName() {
        logger.info("Please specify user Last Name:");
        String name = input.next();
        userService.findByLastName(name).ifPresentOrElse(
                UserController::printCollection,
                UserController::printReadError
        );
    }

    static void findUserByFullName() {
        logger.info("Please specify user First Name:");
        String firsName = input.next();
        logger.info("Please specify user Last Name:");
        String lastName = input.next();
        userService.findByName(firsName, lastName).ifPresentOrElse(
                UserController::printCollection,
                UserController::printReadError
        );
    }

    static void findUserByPhoneNumber() {
        logger.info("Please specify user Phone Number:");
        String number = input.next();
        userService.findByPhoneNumber(number).ifPresentOrElse(
                user -> logger.info(user),
                () -> logger.info("No user with phone number '{}' in database", number)
        );
    }

    static void findUserByTariffId() {
        logger.info("Please specify tariff UUID:");
        String id = input.next();
        userService.findByTariffId(id).ifPresentOrElse(
                UserController::printCollection,
                UserController::printReadError
        );
    }

    static void findUserByTariffName() {
        logger.info("Please specify tariff Name:");
        String name = input.next();
        userService.findByTariffName(name).ifPresentOrElse(
                UserController::printCollection,
                UserController::printReadError
        );
    }

    static void createNewUser() {
        int count = userService.createNewUser(getParams(userService.getNewUserFields()));
        logger.info("Created {} records", count);
    }

    static void updateUserInfo() {
        logger.info("Please specify user UUID:");
        String id = input.next();
        int count = userService.updateUserInfo(id, getParams(userService.getNewUserFields()));
        logger.info("Updated {} records", count);
    }

    static void updateUserPhoneNumber() {
        logger.info("Please specify user UUID:");
        String id = input.next();
        logger.info("Please specify new phone number:");
        String number = input.next();
        int count = userService.updateUserPhoneNumber(id, number);
        logger.info("Updated {} records", count);
    }

    static void updateUserTariffId() {
        logger.info("Please specify user UUID:");
        String userId = input.next();
        logger.info("Please specify tariff UUID:");
        String tariffId = input.next();
        int count = userService.changeTariffId(userId, tariffId);
        logger.info("Updated {} records", count);
    }

    static void deleteUser() {
        logger.info("Please specify user UUID:");
        String userId = input.next();
        int count = userService.deleteUser(userId);
        logger.info("Deleted {} records", count);
    }

    static void addMoneyToAccount() {
        logger.info("Please specify user UUID:");
        String userId = input.next();
        logger.info("How much to add:");
        int money = input.nextInt();
        int count = userService.addMoneyToAccount(userId, money);
        logger.info("Updated {} records", count);
    }

    // TODO: Better to move all such methods to some Util class but I want to leave it here to print all messages by this logger
    private static Map<String, String> getParams(List<String> fields) {
        logger.info("Please specify each parameter for notification.");
        Map<String, String> params = new HashMap<>();
        fields.forEach(field -> {
            logger.info("Specify {}", StringUtils.replace(field, "_", StringUtils.SPACE));
            params.put(field, StringUtils.trim(input.next()));
        });
        return params;
    }

    private static void printCollection(Collection<?> collection) {
        if (CollectionUtils.isEmpty(collection)) {
            logger.info("No users found");
        } else {
            logger.info("Users:");
            collection.forEach(item -> logger.info(item));
        }
    }

    private static void printReadError() {
        logger.error("Cannot find data in database");
    }
}
