package com.epam.lab.mobile_network.dao;

import com.epam.lab.mobile_network.model.Call;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface CallDAO extends BaseDAO<Call, UUID> {
    Optional<List<Call>> findByReceiverId(UUID receiverId);
    Optional<List<Call>> findBySenderId(UUID senderId);
    Optional<List<Call>> findBySenderAndReceiverId(UUID senderId, UUID receiverId);
}
