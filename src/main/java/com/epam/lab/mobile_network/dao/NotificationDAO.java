package com.epam.lab.mobile_network.dao;

import com.epam.lab.mobile_network.model.Notification;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface NotificationDAO extends BaseDAO<Notification, UUID> {
    Optional<List<Notification>> findAllByUserId(UUID userId);
    Optional<List<Notification>> findAllByDate(LocalDateTime upToDate);
}
