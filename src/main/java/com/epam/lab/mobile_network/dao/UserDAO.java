package com.epam.lab.mobile_network.dao;

import com.epam.lab.mobile_network.model.User;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface UserDAO extends BaseDAO<User, UUID> {
    Optional<List<User>> findByFirstName(String firstName);
    Optional<List<User>> findByLastName(String lastName);
    Optional<List<User>> findByName(String firstName, String lastName);
    Optional<User> findByPhoneNumber(String phoneNumber);
    Optional<User> findByAccountId(UUID accountId);
    Optional<List<User>> findByTariffId(UUID tariffId);
    Optional<List<User>> findByTariffName(String tariffName);
}
