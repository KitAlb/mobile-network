package com.epam.lab.mobile_network.dao.implementation;

import com.epam.lab.mobile_network.connection.ConnectionManager;
import com.epam.lab.mobile_network.dao.AccountDAO;
import com.epam.lab.mobile_network.model.Account;
import com.epam.lab.mobile_network.transformer.Transformer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public class AccountDaoImpl implements AccountDAO {
    private static Logger logger = LogManager.getLogger("ConsoleLogger");

    private static final String FIND_ALL = "SELECT * FROM account";
    private static final String DELETE = "DELETE FROM account WHERE id=?";
    private static final String CREATE = "INSERT account (id, money, update_date, free_sms, free_call) VALUES (?, ?, ?, ?, ?)";
    private static final String UPDATE = "UPDATE account SET money=?, update_date=?, free_sms=?, free_call=? WHERE id=?";
    private static final String FIND_BY_ID = "SELECT * FROM account WHERE id=?";
    private static final String FIND_BY_USER_ID = "SELECT a.* FROM account a INNER JOIN user u ON u.account_id = a.id WHERE u.id=?";

    @Override
    public Optional<List<Account>> findAll() {
        return ConnectionManager.getConnection()
                .map(connection -> {
                    List<Account> entities = new ArrayList<>();
                    try (Statement statement = connection.createStatement();
                         ResultSet resultSet = statement.executeQuery(FIND_ALL)) {
                        while (resultSet.next()) {
                            entities.add(new Transformer<>(Account.class).fromResultSetToEntity(resultSet));
                        }
                    } catch (Exception ex) {
                        logger.error(ex.getMessage());
                    }
                    return Optional.of(entities);
                })
                .orElse(Optional.empty());
    }

    @Override
    public Optional<Account> findById(UUID uuid) {
        return ConnectionManager.getConnection()
                .map(connection -> {
                    Account entity = null;
                    try (PreparedStatement ps = connection.prepareStatement(FIND_BY_ID)) {
                        ps.setString(1, uuid.toString());
                        try (ResultSet resultSet = ps.executeQuery()) {
                            while (resultSet.next()) {
                                entity = new Transformer<>(Account.class).fromResultSetToEntity(resultSet);
                                break;
                            }
                        }
                    } catch (Exception ex) {
                        logger.error(ex.getMessage());
                    }
                    return Optional.ofNullable(entity);
                }).orElse(Optional.empty());
    }

    @Override
    public int create(Account entity) {
        return ConnectionManager.getConnection()
                .map(connection -> {
                    int result = 0;
                    try (PreparedStatement ps = connection.prepareStatement(CREATE)) {
                        ps.setString(1, entity.getId().toString());
                        ps.setInt(2, entity.getMoney());
                        ps.setTimestamp(3, Timestamp.valueOf(entity.getUpdateDateTime()));
                        ps.setInt(4, entity.getFreeSms());
                        ps.setInt(5, entity.getFreeCall());
                        result = ps.executeUpdate();
                    } catch (Exception ex) {
                        logger.error(ex.getMessage());
                    }
                    return result;
                }).orElse(0);
    }

    @Override
    public int update(Account entity) {
        return ConnectionManager.getConnection()
                .map(connection -> {
                    int result = 0;
                    try (PreparedStatement ps = connection.prepareStatement(UPDATE)) {
                        ps.setInt(1, entity.getMoney());
                        ps.setTimestamp(2, Timestamp.valueOf(entity.getUpdateDateTime()));
                        ps.setInt(3, entity.getFreeSms());
                        ps.setInt(4, entity.getFreeCall());
                        ps.setString(5, entity.getId().toString());
                        result = ps.executeUpdate();
                    } catch (Exception ex) {
                        logger.error(ex.getMessage());
                    }
                    return result;
                }).orElse(0);
    }

    @Override
    public int delete(UUID uuid) {
        return ConnectionManager.getConnection()
                .map(connection -> {
                    int result = 0;
                    try (PreparedStatement ps = connection.prepareStatement(DELETE)) {
                        ps.setString(1, uuid.toString());
                        result = ps.executeUpdate();
                    } catch (Exception ex) {
                        logger.error(ex.getMessage());
                    }
                    return result;
                }).orElse(0);
    }

    @Override
    public Optional<Account> findByUserId(UUID userId) {
        return ConnectionManager.getConnection()
                .map(connection -> {
                    Account entity = null;
                    try (PreparedStatement ps = connection.prepareStatement(FIND_BY_USER_ID)) {
                        ps.setString(1, userId.toString());
                        try (ResultSet resultSet = ps.executeQuery()) {
                            while (resultSet.next()) {
                                entity = new Transformer<>(Account.class).fromResultSetToEntity(resultSet);
                                break;
                            }
                        }
                    } catch (Exception ex) {
                        logger.error(ex.getMessage());
                    }
                    return Optional.ofNullable(entity);
                }).orElse(Optional.empty());
    }
}
