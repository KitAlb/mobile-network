package com.epam.lab.mobile_network.dao.implementation;

import com.epam.lab.mobile_network.connection.ConnectionManager;
import com.epam.lab.mobile_network.dao.NotificationDAO;
import com.epam.lab.mobile_network.model.Notification;
import com.epam.lab.mobile_network.transformer.Transformer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public class NotificationDaoImpl implements NotificationDAO {
    private static Logger logger = LogManager.getLogger("ConsoleLogger");

    private static final String FIND_ALL = "SELECT * FROM notification";
    private static final String DELETE = "DELETE FROM notification WHERE id=?";
    private static final String CREATE = "INSERT notification " +
            "(id, user_id, trigger_date, notification_text) " +
            "VALUES (?, ?, ?, ?)";
    private static final String UPDATE = "UPDATE notification " +
            "SET user_id=?, trigger_date=?, notification_text=? " +
            "WHERE id=?";
    private static final String FIND_BY_ID = "SELECT * FROM notification WHERE id=?";
    private static final String FIND_BY_USER_ID = "SELECT * FROM notification WHERE user_id=?";
    private static final String FIND_BY_DATE = "SELECT * FROM notification WHERE trigger_date <= date(?)";

    @Override
    public Optional<List<Notification>> findAllByUserId(UUID userId) {
        return ConnectionManager.getConnection()
                .map(connection -> {
                    List<Notification> entities = new ArrayList<>();
                    try (PreparedStatement ps = connection.prepareStatement(FIND_BY_USER_ID)) {
                        ps.setString(1, userId.toString());
                        try (ResultSet resultSet = ps.executeQuery()) {
                            while (resultSet.next()) {
                                entities.add(new Transformer<>(Notification.class).fromResultSetToEntity(resultSet));
                            }
                        }
                    } catch (Exception ex) {
                        logger.error(ex.getMessage());
                    }
                    return Optional.of(entities);
                }).orElse(Optional.empty());
    }

    @Override
    public Optional<List<Notification>> findAllByDate(LocalDateTime upToDate) {
        return ConnectionManager.getConnection()
                .map(connection -> {
                    List<Notification> entities = new ArrayList<>();
                    try (PreparedStatement ps = connection.prepareStatement(FIND_BY_DATE)) {
                        ps.setTimestamp(1, Timestamp.valueOf(upToDate));
                        try (ResultSet resultSet = ps.executeQuery()) {
                            while (resultSet.next()) {
                                entities.add(new Transformer<>(Notification.class).fromResultSetToEntity(resultSet));
                            }
                        }
                    } catch (Exception ex) {
                        logger.error(ex.getMessage());
                    }
                    return Optional.of(entities);
                }).orElse(Optional.empty());
    }

    @Override
    public Optional<List<Notification>> findAll() {
        return ConnectionManager.getConnection()
                .map(connection -> {
                    List<Notification> entities = new ArrayList<>();
                    try (Statement statement = connection.createStatement();
                         ResultSet resultSet = statement.executeQuery(FIND_ALL)) {
                        while (resultSet.next()) {
                            entities.add(new Transformer<>(Notification.class).fromResultSetToEntity(resultSet));
                        }
                    } catch (Exception ex) {
                        logger.error(ex.getMessage());
                    }
                    return Optional.of(entities);
                })
                .orElse(Optional.empty());
    }

    @Override
    public Optional<Notification> findById(UUID uuid) {
        return ConnectionManager.getConnection()
                .map(connection -> {
                    Notification entity = null;
                    try (PreparedStatement ps = connection.prepareStatement(FIND_BY_ID)) {
                        ps.setString(1, uuid.toString());
                        try (ResultSet resultSet = ps.executeQuery()) {
                            while (resultSet.next()) {
                                entity = new Transformer<>(Notification.class).fromResultSetToEntity(resultSet);
                                break;
                            }
                        }
                    } catch (Exception ex) {
                        logger.error(ex.getMessage());
                    }
                    return Optional.ofNullable(entity);
                }).orElse(Optional.empty());
    }

    @Override
    public int create(Notification entity) {
        return ConnectionManager.getConnection()
                .map(connection -> {
                    int result = 0;
                    try (PreparedStatement ps = connection.prepareStatement(CREATE)) {
                        ps.setString(1, entity.getId().toString());
                        ps.setString(2, entity.getUserId().toString());
                        ps.setTimestamp(3, Timestamp.valueOf(entity.getTriggerDate()));
                        ps.setString(4, entity.getNotificationText());
                        result = ps.executeUpdate();
                    } catch (Exception ex) {
                        logger.error(ex.getMessage());
                    }
                    return result;
                }).orElse(0);
    }

    @Override
    public int update(Notification entity) {
        return ConnectionManager.getConnection()
                .map(connection -> {
                    int result = 0;
                    try (PreparedStatement ps = connection.prepareStatement(UPDATE)) {
                        ps.setString(1, entity.getUserId().toString());
                        ps.setTimestamp(2, Timestamp.valueOf(entity.getTriggerDate()));
                        ps.setString(3, entity.getNotificationText());
                        ps.setString(4, entity.getId().toString());
                        result = ps.executeUpdate();
                    } catch (Exception ex) {
                        logger.error(ex.getMessage());
                    }
                    return result;
                }).orElse(0);
    }

    @Override
    public int delete(UUID uuid) {
        return ConnectionManager.getConnection()
                .map(connection -> {
                    int result = 0;
                    try (PreparedStatement ps = connection.prepareStatement(DELETE)) {
                        ps.setString(1, uuid.toString());
                        result = ps.executeUpdate();
                    } catch (Exception ex) {
                        logger.error(ex.getMessage());
                    }
                    return result;
                }).orElse(0);
    }
}
