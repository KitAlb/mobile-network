package com.epam.lab.mobile_network.dao.implementation;

import com.epam.lab.mobile_network.connection.ConnectionManager;
import com.epam.lab.mobile_network.dao.SmsDAO;
import com.epam.lab.mobile_network.model.Sms;
import com.epam.lab.mobile_network.transformer.Transformer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public class SmsDaoImpl implements SmsDAO {
    private static Logger logger = LogManager.getLogger("ConsoleLogger");

    private static final String FIND_ALL = "SELECT * FROM sms";
    private static final String DELETE = "DELETE FROM sms WHERE id=?";
    private static final String CREATE = "INSERT sms " +
            "(id, receiver_id, sender_id, datetime, sms_text) " +
            "VALUES (?, ?, ?, ?, ?)";
    private static final String UPDATE = "UPDATE sms " +
            "SET receiver_id=?, sender_id=?, datetime=?, sms_text=? " +
            "WHERE id=?";
    private static final String FIND_BY_ID = "SELECT * FROM sms WHERE id=?";
    private static final String FIND_BY_SENDER = "SELECT * FROM sms WHERE sender_id=?";
    private static final String FIND_BY_RECEIVER = "SELECT * FROM sms WHERE receiver_id=?";
    private static final String FIND_BY_SENDER_AND_RECEIVER = "SELECT * FROM sms WHERE sender_id=? AND receiver_id=?";

    @Override
    public Optional<List<Sms>> findByReceiverId(UUID receiverId) {
        return ConnectionManager.getConnection()
                .map(connection -> {
                    List<Sms> entities = new ArrayList<>();
                    try (PreparedStatement ps = connection.prepareStatement(FIND_BY_RECEIVER)) {
                        ps.setString(1, receiverId.toString());
                        try (ResultSet resultSet = ps.executeQuery()) {
                            while (resultSet.next()) {
                                entities.add(new Transformer<>(Sms.class).fromResultSetToEntity(resultSet));
                            }
                        }
                    } catch (Exception ex) {
                        logger.error(ex.getMessage());
                    }
                    return Optional.of(entities);
                }).orElse(Optional.empty());
    }

    @Override
    public Optional<List<Sms>> findBySenderId(UUID senderId) {
        return ConnectionManager.getConnection()
                .map(connection -> {
                    List<Sms> entities = new ArrayList<>();
                    try (PreparedStatement ps = connection.prepareStatement(FIND_BY_SENDER)) {
                        ps.setString(1, senderId.toString());
                        try (ResultSet resultSet = ps.executeQuery()) {
                            while (resultSet.next()) {
                                entities.add(new Transformer<>(Sms.class).fromResultSetToEntity(resultSet));
                            }
                        }
                    } catch (Exception ex) {
                        logger.error(ex.getMessage());
                    }
                    return Optional.of(entities);
                }).orElse(Optional.empty());
    }

    @Override
    public Optional<List<Sms>> findBySenderAndReceiverId(UUID senderId, UUID receiverId) {
        return ConnectionManager.getConnection()
                .map(connection -> {
                    List<Sms> entities = new ArrayList<>();
                    try (PreparedStatement ps = connection.prepareStatement(FIND_BY_SENDER_AND_RECEIVER)) {
                        ps.setString(1, senderId.toString());
                        ps.setString(2, receiverId.toString());
                        try (ResultSet resultSet = ps.executeQuery()) {
                            while (resultSet.next()) {
                                entities.add(new Transformer<>(Sms.class).fromResultSetToEntity(resultSet));
                            }
                        }
                    } catch (Exception ex) {
                        logger.error(ex.getMessage());
                    }
                    return Optional.of(entities);
                }).orElse(Optional.empty());
    }

    @Override
    public Optional<List<Sms>> findAll() {
        return ConnectionManager.getConnection()
                .map(connection -> {
                    List<Sms> entities = new ArrayList<>();
                    try (Statement statement = connection.createStatement();
                         ResultSet resultSet = statement.executeQuery(FIND_ALL)) {
                        while (resultSet.next()) {
                            entities.add(new Transformer<>(Sms.class).fromResultSetToEntity(resultSet));
                        }
                    } catch (Exception ex) {
                        logger.error(ex.getMessage());
                    }
                    return Optional.of(entities);
                })
                .orElse(Optional.empty());
    }

    @Override
    public Optional<Sms> findById(UUID uuid) {
        return ConnectionManager.getConnection()
                .map(connection -> {
                    Sms entity = null;
                    try (PreparedStatement ps = connection.prepareStatement(FIND_BY_ID)) {
                        ps.setString(1, uuid.toString());
                        try (ResultSet resultSet = ps.executeQuery()) {
                            while (resultSet.next()) {
                                entity = new Transformer<>(Sms.class).fromResultSetToEntity(resultSet);
                                break;
                            }
                        }
                    } catch (Exception ex) {
                        logger.error(ex.getMessage());
                    }
                    return Optional.ofNullable(entity);
                }).orElse(Optional.empty());
    }

    @Override
    public int create(Sms entity) {
        return ConnectionManager.getConnection()
                .map(connection -> {
                    int result = 0;
                    try (PreparedStatement ps = connection.prepareStatement(CREATE)) {
                        ps.setString(1, entity.getId().toString());
                        ps.setString(2, entity.getReceiverId().toString());
                        ps.setString(3, entity.getSenderId().toString());
                        ps.setTimestamp(4, Timestamp.valueOf(entity.getDateTime()));
                        ps.setString(5, entity.getSmsText());
                        result = ps.executeUpdate();
                    } catch (Exception ex) {
                        logger.error(ex.getMessage());
                    }
                    return result;
                }).orElse(0);
    }

    @Override
    public int update(Sms entity) {
        return ConnectionManager.getConnection()
                .map(connection -> {
                    int result = 0;
                    try (PreparedStatement ps = connection.prepareStatement(UPDATE)) {
                        ps.setString(1, entity.getReceiverId().toString());
                        ps.setString(2, entity.getSenderId().toString());
                        ps.setTimestamp(3, Timestamp.valueOf(entity.getDateTime()));
                        ps.setString(4, entity.getSmsText());
                        ps.setString(5, entity.getId().toString());
                        result = ps.executeUpdate();
                    } catch (Exception ex) {
                        logger.error(ex.getMessage());
                    }
                    return result;
                }).orElse(0);
    }

    @Override
    public int delete(UUID uuid) {
        return ConnectionManager.getConnection()
                .map(connection -> {
                    int result = 0;
                    try (PreparedStatement ps = connection.prepareStatement(DELETE)) {
                        ps.setString(1, uuid.toString());
                        result = ps.executeUpdate();
                    } catch (Exception ex) {
                        logger.error(ex.getMessage());
                    }
                    return result;
                }).orElse(0);
    }
}
