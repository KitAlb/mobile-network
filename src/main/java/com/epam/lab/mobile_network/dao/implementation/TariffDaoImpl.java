package com.epam.lab.mobile_network.dao.implementation;

import com.epam.lab.mobile_network.connection.ConnectionManager;
import com.epam.lab.mobile_network.dao.TariffDAO;
import com.epam.lab.mobile_network.model.Tariff;
import com.epam.lab.mobile_network.model.type.TariffType;
import com.epam.lab.mobile_network.transformer.Transformer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public class TariffDaoImpl implements TariffDAO {
    private static Logger logger = LogManager.getLogger("ConsoleLogger");

    private static final String FIND_ALL = "SELECT * FROM tariff";
    private static final String DELETE = "DELETE FROM tariff WHERE id=?";
    private static final String CREATE = "INSERT tariff " +
            "(id, name, sms_cost, call_cost, free_sms_count, free_call_count, type) " +
            "VALUES (?, ?, ?, ?, ?, ?, ?)";
    private static final String UPDATE = "UPDATE tariff " +
            "SET name=?, sms_cost=?, call_cost=?, free_sms_count=?, free_call_count=?, type=? " +
            "WHERE id=?";
    private static final String FIND_BY_ID = "SELECT * FROM tariff WHERE id=?";
    private static final String FIND_BY_NAME = "SELECT * FROM tariff WHERE name=?";
    private static final String FIND_BY_TYPE = "SELECT * FROM tariff WHERE type=?";
    private static final String FIND_BY_USER_ID = "SELECT t.* FROM tariff t INNER JOIN user u ON u.tariff_id=t.id WHERE u.id=?";

    @Override
    public Optional<List<Tariff>> findByName(String name) {
        return ConnectionManager.getConnection()
                .map(connection -> {
                    List<Tariff> entities = new ArrayList<>();
                    try (PreparedStatement ps = connection.prepareStatement(FIND_BY_NAME)) {
                        ps.setString(1, name);
                        try (ResultSet resultSet = ps.executeQuery()) {
                            while (resultSet.next()) {
                                entities.add(new Transformer<>(Tariff.class).fromResultSetToEntity(resultSet));
                            }
                        }
                    } catch (Exception ex) {
                        logger.error(ex.getMessage());
                    }
                    return Optional.of(entities);
                }).orElse(Optional.empty());
    }

    @Override
    public Optional<List<Tariff>> findByType(TariffType type) {
        return ConnectionManager.getConnection()
                .map(connection -> {
                    List<Tariff> entities = new ArrayList<>();
                    try (PreparedStatement ps = connection.prepareStatement(FIND_BY_TYPE)) {
                        ps.setString(1, type.name());
                        try (ResultSet resultSet = ps.executeQuery()) {
                            while (resultSet.next()) {
                                entities.add(new Transformer<>(Tariff.class).fromResultSetToEntity(resultSet));
                            }
                        }
                    } catch (Exception ex) {
                        logger.error(ex.getMessage());
                    }
                    return Optional.of(entities);
                }).orElse(Optional.empty());
    }

    @Override
    public Optional<Tariff> findByUserId(UUID userId) {
        return ConnectionManager.getConnection()
                .map(connection -> {
                    Tariff entity = null;
                    try (PreparedStatement ps = connection.prepareStatement(FIND_BY_USER_ID)) {
                        ps.setString(1, userId.toString());
                        try (ResultSet resultSet = ps.executeQuery()) {
                            while (resultSet.next()) {
                                entity = new Transformer<>(Tariff.class).fromResultSetToEntity(resultSet);
                                break;
                            }
                        }
                    } catch (Exception ex) {
                        logger.error(ex.getMessage());
                    }
                    return Optional.ofNullable(entity);
                }).orElse(Optional.empty());
    }

    @Override
    public Optional<List<Tariff>> findAll() {
        return ConnectionManager.getConnection()
                .map(connection -> {
                    List<Tariff> entities = new ArrayList<>();
                    try (Statement statement = connection.createStatement();
                         ResultSet resultSet = statement.executeQuery(FIND_ALL)) {
                        while (resultSet.next()) {
                            entities.add(new Transformer<>(Tariff.class).fromResultSetToEntity(resultSet));
                        }
                    } catch (Exception ex) {
                        logger.error(ex.getMessage());
                    }
                    return Optional.of(entities);
                })
                .orElse(Optional.empty());
    }

    @Override
    public Optional<Tariff> findById(UUID uuid) {
        return ConnectionManager.getConnection()
                .map(connection -> {
                    Tariff entity = null;
                    try (PreparedStatement ps = connection.prepareStatement(FIND_BY_ID)) {
                        ps.setString(1, uuid.toString());
                        try (ResultSet resultSet = ps.executeQuery()) {
                            while (resultSet.next()) {
                                entity = new Transformer<>(Tariff.class).fromResultSetToEntity(resultSet);
                                break;
                            }
                        }
                    } catch (Exception ex) {
                        logger.error(ex.getMessage());
                    }
                    return Optional.ofNullable(entity);
                }).orElse(Optional.empty());
    }

    @Override
    public int create(Tariff entity) {
        return ConnectionManager.getConnection()
                .map(connection -> {
                    int result = 0;
                    try (PreparedStatement ps = connection.prepareStatement(CREATE)) {
                        ps.setString(1, entity.getId().toString());
                        ps.setString(2, entity.getName());
                        ps.setInt(3, entity.getSmsCost());
                        ps.setInt(4, entity.getCallCost());
                        ps.setInt(5, entity.getFreeSmsCount());
                        ps.setInt(6, entity.getFreeCallCount());
                        ps.setString(7, entity.getTariffType().name());
                        result = ps.executeUpdate();
                    } catch (Exception ex) {
                        logger.error(ex.getMessage());
                    }
                    return result;
                }).orElse(0);
    }

    @Override
    public int update(Tariff entity) {
        return ConnectionManager.getConnection()
                .map(connection -> {
                    int result = 0;
                    try (PreparedStatement ps = connection.prepareStatement(UPDATE)) {
                        ps.setString(1, entity.getName());
                        ps.setInt(2, entity.getSmsCost());
                        ps.setInt(3, entity.getCallCost());
                        ps.setInt(4, entity.getFreeSmsCount());
                        ps.setInt(5, entity.getFreeCallCount());
                        ps.setString(6, entity.getTariffType().name());
                        ps.setString(7, entity.getId().toString());
                        result = ps.executeUpdate();
                    } catch (Exception ex) {
                        logger.error(ex.getMessage());
                    }
                    return result;
                }).orElse(0);
    }

    @Override
    public int delete(UUID uuid) {
        return ConnectionManager.getConnection()
                .map(connection -> {
                    int result = 0;
                    try (PreparedStatement ps = connection.prepareStatement(DELETE)) {
                        ps.setString(1, uuid.toString());
                        result = ps.executeUpdate();
                    } catch (Exception ex) {
                        logger.error(ex.getMessage());
                    }
                    return result;
                }).orElse(0);
    }
}
