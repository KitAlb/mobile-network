package com.epam.lab.mobile_network.dao.implementation;

import com.epam.lab.mobile_network.connection.ConnectionManager;
import com.epam.lab.mobile_network.dao.UserDAO;
import com.epam.lab.mobile_network.model.User;
import com.epam.lab.mobile_network.transformer.Transformer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public class UserDaoImpl implements UserDAO {
    private static Logger logger = LogManager.getLogger("ConsoleLogger");

    private static final String FIND_ALL = "SELECT * FROM user";
    private static final String DELETE = "DELETE FROM user WHERE id=?";
    private static final String CREATE = "INSERT user " +
            "(id, first_name, last_name, tariff_id, phone_number, account_id) " +
            "VALUES (?, ?, ?, ?, ?, ?)";
    private static final String UPDATE = "UPDATE user " +
            "SET first_name=?, last_name=?, tariff_id=?, phone_number=?, account_id=? " +
            "WHERE id=?";
    private static final String FIND_BY_ID = "SELECT * FROM user WHERE id=?";
    private static final String FIND_BY_FIRST_NAME = "SELECT * FROM user WHERE first_name=?";
    private static final String FIND_BY_LAST_NAME = "SELECT * FROM user WHERE last_name=?";
    private static final String FIND_BY_NAME = "SELECT * FROM user WHERE first_name=? AND last_name=?";
    private static final String FIND_BY_PHONE_NUMBER = "SELECT * FROM user WHERE phone_number=?";
    private static final String FIND_BY_ACCOUNT_ID = "SELECT * FROM user WHERE account_id=?";
    private static final String FIND_BY_TARIFF_ID = "SELECT * FROM user WHERE tariff_id=?";
    private static final String FIND_BY_TARIFF_NAME = "SELECT u.* FROM user u INNER JOIN tariff t ON u.tariff_id = t.id WHERE t.name=?";

    @Override
    public Optional<List<User>> findByFirstName(String firstName) {
        return ConnectionManager.getConnection()
                .map(connection -> {
                    List<User> entities = new ArrayList<>();
                    try (PreparedStatement ps = connection.prepareStatement(FIND_BY_FIRST_NAME)) {
                        ps.setString(1, firstName);
                        try (ResultSet resultSet = ps.executeQuery()) {
                            while (resultSet.next()) {
                                entities.add(new Transformer<>(User.class).fromResultSetToEntity(resultSet));
                            }
                        }
                    } catch (Exception ex) {
                        logger.error(ex.getMessage());
                    }
                    return Optional.of(entities);
                }).orElse(Optional.empty());
    }

    @Override
    public Optional<List<User>> findByLastName(String lastName) {
        return ConnectionManager.getConnection()
                .map(connection -> {
                    List<User> entities = new ArrayList<>();
                    try (PreparedStatement ps = connection.prepareStatement(FIND_BY_LAST_NAME)) {
                        ps.setString(1, lastName);
                        try (ResultSet resultSet = ps.executeQuery()) {
                            while (resultSet.next()) {
                                entities.add(new Transformer<>(User.class).fromResultSetToEntity(resultSet));
                            }
                        }
                    } catch (Exception ex) {
                        logger.error(ex.getMessage());
                    }
                    return Optional.of(entities);
                }).orElse(Optional.empty());
    }

    @Override
    public Optional<List<User>> findByName(String firstName, String lastName) {
        return ConnectionManager.getConnection()
                .map(connection -> {
                    List<User> entities = new ArrayList<>();
                    try (PreparedStatement ps = connection.prepareStatement(FIND_BY_NAME)) {
                        ps.setString(1, firstName);
                        ps.setString(2, lastName);
                        try (ResultSet resultSet = ps.executeQuery()) {
                            while (resultSet.next()) {
                                entities.add(new Transformer<>(User.class).fromResultSetToEntity(resultSet));
                            }
                        }
                    } catch (Exception ex) {
                        logger.error(ex.getMessage());
                    }
                    return Optional.of(entities);
                }).orElse(Optional.empty());
    }

    @Override
    public Optional<User> findByPhoneNumber(String phoneNumber) {
        return ConnectionManager.getConnection()
                .map(connection -> {
                    User entity = null;
                    try (PreparedStatement ps = connection.prepareStatement(FIND_BY_PHONE_NUMBER)) {
                        ps.setString(1, phoneNumber);
                        try (ResultSet resultSet = ps.executeQuery()) {
                            while (resultSet.next()) {
                                entity = new Transformer<>(User.class).fromResultSetToEntity(resultSet);
                                break;
                            }
                        }
                    } catch (Exception ex) {
                        logger.error(ex.getMessage());
                    }
                    return Optional.ofNullable(entity);
                }).orElse(Optional.empty());
    }

    @Override
    public Optional<User> findByAccountId(UUID accountId) {
        return ConnectionManager.getConnection()
                .map(connection -> {
                    User entity = null;
                    try (PreparedStatement ps = connection.prepareStatement(FIND_BY_ACCOUNT_ID)) {
                        ps.setString(1, accountId.toString());
                        try (ResultSet resultSet = ps.executeQuery()) {
                            while (resultSet.next()) {
                                entity = new Transformer<>(User.class).fromResultSetToEntity(resultSet);
                                break;
                            }
                        }
                    } catch (Exception ex) {
                        logger.error(ex.getMessage());
                    }
                    return Optional.ofNullable(entity);
                }).orElse(Optional.empty());
    }

    @Override
    public Optional<List<User>> findByTariffId(UUID tariffId) {
        return ConnectionManager.getConnection()
                .map(connection -> {
                    List<User> entities = new ArrayList<>();
                    try (PreparedStatement ps = connection.prepareStatement(FIND_BY_TARIFF_ID)) {
                        ps.setString(1, tariffId.toString());
                        try (ResultSet resultSet = ps.executeQuery()) {
                            while (resultSet.next()) {
                                entities.add(new Transformer<>(User.class).fromResultSetToEntity(resultSet));
                            }
                        }
                    } catch (Exception ex) {
                        logger.error(ex.getMessage());
                    }
                    return Optional.of(entities);
                }).orElse(Optional.empty());
    }

    @Override
    public Optional<List<User>> findByTariffName(String tariffName) {
        return ConnectionManager.getConnection()
                .map(connection -> {
                    List<User> entities = new ArrayList<>();
                    try (PreparedStatement ps = connection.prepareStatement(FIND_BY_TARIFF_NAME)) {
                        ps.setString(1, tariffName);
                        try (ResultSet resultSet = ps.executeQuery()) {
                            while (resultSet.next()) {
                                entities.add(new Transformer<>(User.class).fromResultSetToEntity(resultSet));
                            }
                        }
                    } catch (Exception ex) {
                        logger.error(ex.getMessage());
                    }
                    return Optional.of(entities);
                }).orElse(Optional.empty());
    }

    @Override
    public Optional<List<User>> findAll() {
        return ConnectionManager.getConnection()
                .map(connection -> {
                    List<User> entities = new ArrayList<>();
                    try (Statement statement = connection.createStatement();
                         ResultSet resultSet = statement.executeQuery(FIND_ALL)) {
                        while (resultSet.next()) {
                            entities.add(new Transformer<>(User.class).fromResultSetToEntity(resultSet));
                        }
                    } catch (Exception ex) {
                        logger.error(ex.getMessage());
                    }
                    return Optional.of(entities);
                })
                .orElse(Optional.empty());
    }

    @Override
    public Optional<User> findById(UUID uuid) {
        return ConnectionManager.getConnection()
                .map(connection -> {
                    User entity = null;
                    try (PreparedStatement ps = connection.prepareStatement(FIND_BY_ID)) {
                        ps.setString(1, uuid.toString());
                        try (ResultSet resultSet = ps.executeQuery()) {
                            while (resultSet.next()) {
                                entity = new Transformer<>(User.class).fromResultSetToEntity(resultSet);
                                break;
                            }
                        }
                    } catch (Exception ex) {
                        logger.error(ex.getMessage());
                    }
                    return Optional.ofNullable(entity);
                }).orElse(Optional.empty());
    }

    @Override
    public int create(User entity) {
        return ConnectionManager.getConnection()
                .map(connection -> {
                    int result = 0;
                    try (PreparedStatement ps = connection.prepareStatement(CREATE)) {
                        ps.setString(1, entity.getId().toString());
                        ps.setString(2, entity.getFirstName());
                        ps.setString(3, entity.getLastName());
                        ps.setString(4, entity.getTariffId().toString());
                        ps.setString(5, entity.getPhoneNumber());
                        ps.setString(6, entity.getAccountId().toString());
                        result = ps.executeUpdate();
                    } catch (Exception ex) {
                        logger.error(ex.getMessage());
                    }
                    return result;
                }).orElse(0);
    }

    @Override
    public int update(User entity) {
        return ConnectionManager.getConnection()
                .map(connection -> {
                    int result = 0;
                    try (PreparedStatement ps = connection.prepareStatement(UPDATE)) {
                        ps.setString(1, entity.getFirstName());
                        ps.setString(2, entity.getLastName());
                        ps.setString(3, entity.getTariffId().toString());
                        ps.setString(4, entity.getPhoneNumber());
                        ps.setString(5, entity.getAccountId().toString());
                        ps.setString(6, entity.getId().toString());
                        result = ps.executeUpdate();
                    } catch (Exception ex) {
                        logger.error(ex.getMessage());
                    }
                    return result;
                }).orElse(0);
    }

    @Override
    public int delete(UUID uuid) {
        return ConnectionManager.getConnection()
                .map(connection -> {
                    int result = 0;
                    try (PreparedStatement ps = connection.prepareStatement(DELETE)) {
                        ps.setString(1, uuid.toString());
                        result = ps.executeUpdate();
                    } catch (Exception ex) {
                        logger.error(ex.getMessage());
                    }
                    return result;
                }).orElse(0);
    }
}
