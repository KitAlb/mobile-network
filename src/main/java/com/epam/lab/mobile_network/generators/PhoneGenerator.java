package com.epam.lab.mobile_network.generators;

import org.apache.commons.lang3.StringUtils;

import java.util.Random;

public class PhoneGenerator {
    private static Random generator = new Random();
    // TODO: replace with smart generator that will check database for already existing numbers
    public static String generatePhoneNumber() {
        int set1 = generator.nextInt(899) + 100;
        int set2 = generator.nextInt(8999) + 1000;
        return StringUtils.join(set1, set2);
    }
}
