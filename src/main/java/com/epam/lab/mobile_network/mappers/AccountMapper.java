package com.epam.lab.mobile_network.mappers;

import com.epam.lab.mobile_network.model.Account;

public class AccountMapper {
    public static Account updateFreebies(Account account, Integer smsCount, Integer callsCount) {
        return Account.builder()
                .id(account.getId())
                .money(account.getMoney())
                .updateDateTime(account.getUpdateDateTime())
                .freeSms(smsCount)
                .freeCall(callsCount)
                .build();
    }

    public static Account updateMoney(Account account, Integer money) {
        return Account.builder()
                .id(account.getId())
                .updateDateTime(account.getUpdateDateTime())
                .freeCall(account.getFreeCall())
                .freeSms(account.getFreeSms())
                .money(money)
                .build();
    }

}
