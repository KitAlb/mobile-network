package com.epam.lab.mobile_network.model;

import com.epam.lab.mobile_network.model.annotation.PrimaryKey;
import com.epam.lab.mobile_network.model.annotation.Column;
import com.epam.lab.mobile_network.model.annotation.Table;

import java.time.LocalDateTime;
import java.util.UUID;

@Table(name = "account")
public class Account {
    private static final Integer DEFAULT_MONEY = 0;
    private static final Integer DEFAULT_FREE_SMS = 0;
    private static final Integer DEFAULT_FREE_CALL = 0;

    @PrimaryKey
    @Column(name = "id")
    private UUID id;
    @Column(name = "money")
    private Integer money;
    @Column(name = "update_date")
    private LocalDateTime updateDateTime;
    @Column(name = "free_sms")
    private Integer freeSms;
    @Column(name = "free_call")
    private Integer freeCall;

    private Account() {
    }

    Account(UUID id, Integer money, LocalDateTime updateDateTime, Integer freeSms, Integer freeCall) {
        this.id = id;
        this.money = money;
        this.updateDateTime = updateDateTime;
        this.freeSms = freeSms;
        this.freeCall = freeCall;
    }

    public static AccountBuilder builder() {
        return new AccountBuilder();
    }

    public UUID getId() {
        return id;
    }

    public Integer getMoney() {
        return money;
    }

    public LocalDateTime getUpdateDateTime() {
        return updateDateTime;
    }

    public Integer getFreeSms() {
        return freeSms;
    }

    public Integer getFreeCall() {
        return freeCall;
    }

    @Override
    public String toString() {
        return "Account{" +
                "id=" + id +
                ", money=" + money +
                ", updateDateTime=" + updateDateTime +
                ", freeSms=" + freeSms +
                ", freeCall=" + freeCall +
                '}';
    }

    public static class AccountBuilder {
        private UUID id;
        private boolean isIdSet;
        private Integer money;
        private boolean isMoneySet;
        private LocalDateTime updateDateTime;
        private boolean isUpdateDateTimeSet;
        private Integer freeSms;
        private boolean isFreeSmsSet;
        private Integer freeCall;
        private boolean isFreeCallSet;

        public Account.AccountBuilder id(UUID id) {
            this.id = id;
            this.isIdSet = true;
            return this;
        }

        public Account.AccountBuilder money(Integer money) {
            this.money = money;
            this.isMoneySet = true;
            return this;
        }

        public Account.AccountBuilder updateDateTime(LocalDateTime updateDateTime) {
            this.updateDateTime = updateDateTime;
            this.isUpdateDateTimeSet = true;
            return this;
        }

        public Account.AccountBuilder freeSms(Integer freeSms) {
            this.freeSms = freeSms;
            this.isFreeSmsSet = true;
            return this;
        }

        public Account.AccountBuilder freeCall(Integer freeCall) {
            this.freeCall = freeCall;
            this.isFreeCallSet = true;
            return this;
        }

        public Account build() {
            UUID id = this.id;
            if (!isIdSet) {
                id = UUID.randomUUID();
            }

            Integer money = this.money;
            if (!isMoneySet) {
                money = DEFAULT_MONEY;
            }

            LocalDateTime dateTime = this.updateDateTime;
            if (!isUpdateDateTimeSet) {
                dateTime = LocalDateTime.now();
            }

            Integer freeSms = this.freeSms;
            if (!isFreeSmsSet) {
                freeSms = DEFAULT_FREE_SMS;
            }

            Integer freeCall = this.freeCall;
            if (!isFreeCallSet) {
                freeCall = DEFAULT_FREE_CALL;
            }

            return new Account(id, money, dateTime, freeSms, freeCall);
        }
    }
}
