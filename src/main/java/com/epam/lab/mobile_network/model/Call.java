package com.epam.lab.mobile_network.model;

import com.epam.lab.mobile_network.model.annotation.PrimaryKey;
import com.epam.lab.mobile_network.model.annotation.Column;
import com.epam.lab.mobile_network.model.annotation.Table;

import java.time.LocalDateTime;
import java.util.UUID;

@Table(name = "call")
public class Call {
    @PrimaryKey
    @Column(name = "id")
    private UUID id;
    @Column(name = "receiver_id")
    private UUID receiverId;
    @Column(name = "sender_id")
    private UUID senderId;
    @Column(name = "datetime")
    private LocalDateTime dateTime;

    private Call() {
    }

    Call(UUID id, UUID receiverId, UUID senderId, LocalDateTime dateTime) {
        this.id = id;
        this.receiverId = receiverId;
        this.senderId = senderId;
        this.dateTime = dateTime;
    }

    public static CallBuilder builder() {
        return new CallBuilder();
    }

    public UUID getId() {
        return id;
    }

    public UUID getReceiverId() {
        return receiverId;
    }

    public UUID getSenderId() {
        return senderId;
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    @Override
    public String toString() {
        return "Call{" +
                "id=" + id +
                ", receiverId=" + receiverId +
                ", senderId=" + senderId +
                ", dateTime=" + dateTime +
                '}';
    }

    public static class CallBuilder {
        private UUID id;
        private boolean isIdSet;
        private UUID receiverId;
        private UUID senderId;
        private LocalDateTime dateTime;
        private boolean isDateTimeSet;

        public CallBuilder id(UUID id) {
            this.id = id;
            this.isIdSet = true;
            return this;
        }

        public CallBuilder receiverId(UUID receiverId) {
            this.receiverId = receiverId;
            return this;
        }

        public CallBuilder senderId(UUID senderId) {
            this.senderId = senderId;
            return this;
        }

        public CallBuilder dateTime(LocalDateTime dateTime) {
            this.dateTime = dateTime;
            this.isDateTimeSet = true;
            return this;
        }

        public Call build() {
            UUID id = this.id;
            if (!isIdSet) {
                id = UUID.randomUUID();
            }

            LocalDateTime dateTime = this.dateTime;
            if (!isDateTimeSet) {
                dateTime = LocalDateTime.now();
            }

            return new Call(id, this.receiverId, this.senderId, dateTime);
        }
    }
}
