package com.epam.lab.mobile_network.model;

import com.epam.lab.mobile_network.model.annotation.PrimaryKey;
import com.epam.lab.mobile_network.model.annotation.Column;
import com.epam.lab.mobile_network.model.annotation.Table;

import java.time.LocalDateTime;
import java.util.UUID;

@Table(name = "sms")
public class Sms {
    @PrimaryKey
    @Column(name = "id")
    private UUID id;
    @Column(name = "receiver_id")
    private UUID receiverId;
    @Column(name = "sender_id")
    private UUID senderId;
    @Column(name = "datetime")
    private LocalDateTime dateTime;
    @Column(name = "sms_text")
    private String smsText;

    private Sms() {
    }

    Sms(UUID id, UUID receiverId, UUID senderId, LocalDateTime dateTime, String smsText) {
        this.id = id;
        this.receiverId = receiverId;
        this.senderId = senderId;
        this.dateTime = dateTime;
        this.smsText = smsText;
    }

    public static SmsBuilder builder() {
        return new SmsBuilder();
    }

    public UUID getId() {
        return id;
    }

    public UUID getReceiverId() {
        return receiverId;
    }

    public UUID getSenderId() {
        return senderId;
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public String getSmsText() {
        return smsText;
    }

    @Override
    public String toString() {
        return "Sms{" +
                "id=" + id +
                ", receiverId=" + receiverId +
                ", senderId=" + senderId +
                ", dateTime=" + dateTime +
                ", smsText='" + smsText + '\'' +
                '}';
    }

    public static class SmsBuilder {
        private UUID id;
        private boolean isIdSet;
        private UUID receiverId;
        private UUID senderId;
        private LocalDateTime dateTime;
        private boolean isDateTimeSet;
        private String smsText;

        public Sms.SmsBuilder id(UUID id) {
            this.id = id;
            this.isIdSet = true;
            return this;
        }

        public Sms.SmsBuilder receiverId(UUID receiverId) {
            this.receiverId = receiverId;
            return this;
        }

        public Sms.SmsBuilder senderId(UUID senderId) {
            this.senderId = senderId;
            return this;
        }

        public Sms.SmsBuilder dateTime(LocalDateTime dateTime) {
            this.dateTime = dateTime;
            this.isDateTimeSet = true;
            return this;
        }

        public Sms.SmsBuilder smsText(String smsText) {
            this.smsText = smsText;
            return this;
        }

        public Sms build() {
            UUID id = this.id;
            if (!isIdSet) {
                id = UUID.randomUUID();
            }

            LocalDateTime dateTime = this.dateTime;
            if (!isDateTimeSet) {
                dateTime = LocalDateTime.now();
            }

            return new Sms(id, this.receiverId, this.senderId, dateTime, this.smsText);
        }
    }
}
