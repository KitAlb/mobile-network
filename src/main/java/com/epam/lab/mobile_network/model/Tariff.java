package com.epam.lab.mobile_network.model;

import com.epam.lab.mobile_network.model.annotation.Table;
import com.epam.lab.mobile_network.model.annotation.Column;
import com.epam.lab.mobile_network.model.annotation.PrimaryKey;
import com.epam.lab.mobile_network.model.type.TariffType;

import java.util.UUID;

@Table(name = "tariff")
public class Tariff {
    @PrimaryKey
    @Column(name = "id", length = 36)
    private UUID id;
    @Column(name = "name", length = 30)
    private String name;
    @Column(name = "sms_cost")
    private Integer smsCost;
    @Column(name = "call_cost")
    private Integer callCost;
    @Column(name = "free_sms_count")
    private Integer freeSmsCount;
    @Column(name = "free_call_count")
    private Integer freeCallCount;
    @Column(name = "type", length = 15)
    private TariffType tariffType;

    private Tariff() {
    }

    Tariff(UUID id, String name, Integer smsCost, Integer callCost, Integer freeSmsCount, Integer freeCallCount, TariffType type) {
        this.id = id;
        this.name = name;
        this.smsCost = smsCost;
        this.callCost = callCost;
        this.freeSmsCount = freeSmsCount;
        this.freeCallCount = freeCallCount;
        this.tariffType = type;
    }

    public UUID getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Integer getSmsCost() {
        return smsCost;
    }

    public Integer getCallCost() {
        return callCost;
    }

    public Integer getFreeSmsCount() {
        return freeSmsCount;
    }

    public Integer getFreeCallCount() {
        return freeCallCount;
    }

    public TariffType getTariffType() {
        return tariffType;
    }

    public static TariffBuilder builder() {
        return new TariffBuilder();
    }

    @Override
    public String toString() {
        return "Tariff{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", smsCost=" + smsCost +
                ", callCost=" + callCost +
                ", freeSmsCount=" + freeSmsCount +
                ", freeCallCount=" + freeCallCount +
                ", tariffType=" + tariffType +
                '}';
    }

    public static class TariffBuilder {
        private UUID id;
        private boolean isIdSet;
        private String name;
        private Integer smsCost;
        private Integer callCost;
        private Integer freeSmsCount;
        private Integer freeCallCount;
        private TariffType tariffType;

        TariffBuilder() {
        }

        public Tariff.TariffBuilder id(UUID id) {
            this.id = id;
            this.isIdSet = true;
            return this;
        }

        public Tariff.TariffBuilder name(String name) {
            this.name = name;
            return this;
        }

        public Tariff.TariffBuilder smsCost(Integer smsCost) {
            this.smsCost = smsCost;
            return this;
        }

        public Tariff.TariffBuilder callCost(Integer callCost) {
            this.callCost = callCost;
            return this;
        }

        public Tariff.TariffBuilder freeSmsCount(Integer freeSmsCount) {
            this.freeSmsCount = freeSmsCount;
            return this;
        }

        public Tariff.TariffBuilder freeCallCount(Integer freeCallCount) {
            this.freeCallCount = freeCallCount;
            return this;
        }

        public Tariff.TariffBuilder tariffType(TariffType tariffType) {
            this.tariffType = tariffType;
            return this;
        }

        public Tariff build() {
            UUID id = this.id;
            if(!isIdSet) {
                id = UUID.randomUUID();
            }
            return new Tariff(
                    id,
                    this.name,
                    this.smsCost,
                    this.callCost,
                    this.freeSmsCount,
                    this.freeCallCount,
                    this.tariffType
            );
        }
    }

}
