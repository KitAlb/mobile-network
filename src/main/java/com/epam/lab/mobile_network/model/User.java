package com.epam.lab.mobile_network.model;

import com.epam.lab.mobile_network.model.annotation.PrimaryKey;
import com.epam.lab.mobile_network.model.annotation.Column;
import com.epam.lab.mobile_network.model.annotation.Table;

import java.util.UUID;

@Table(name = "user")
public class User {
    @PrimaryKey
    @Column(name = "id")
    private UUID id;
    @Column(name = "first_name")
    private String firstName;
    @Column(name = "last_name")
    private String lastName;
    @Column(name = "tariff_id")
    private UUID tariffId;
    @Column(name = "phone_number")
    private String phoneNumber;
    @Column(name = "account_id")
    private UUID accountId;

    private User() {
    }

    User(UUID id, String firstName, String lastName, UUID tariffId, String phoneNumber, UUID accountId) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.tariffId = tariffId;
        this.phoneNumber = phoneNumber;
        this.accountId = accountId;
    }

    public static UserBuilder builder() {
        return new UserBuilder();
    }

    public UUID getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public UUID getTariffId() {
        return tariffId;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public UUID getAccountId() {
        return accountId;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", tariffId=" + tariffId +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", accountId=" + accountId +
                '}';
    }

    public static class UserBuilder {
        private UUID id;
        private boolean isIdSet;
        private String firstName;
        private String lastName;
        private UUID tariffId;
        private String phoneNumber;
        private UUID accountId;

        public User.UserBuilder id(UUID id) {
            this.id = id;
            this.isIdSet = true;
            return this;
        }

        public User.UserBuilder firstName(String firstName) {
            this.firstName = firstName;
            return this;
        }

        public User.UserBuilder lastName(String lastName) {
            this.lastName = lastName;
            return this;
        }

        public User.UserBuilder tariffId(UUID tariffId) {
            this.tariffId = tariffId;
            return this;
        }

        public User.UserBuilder phoneNumber(String phoneNumber) {
            this.phoneNumber = phoneNumber;
            return this;
        }

        public User.UserBuilder accountId(UUID accountId) {
            this.accountId = accountId;
            return this;
        }

        public User build() {
            UUID id = this.id;
            if(!isIdSet) {
                id = UUID.randomUUID();
            }

            return new User(id, this.firstName, this.lastName, this.tariffId, this.phoneNumber, this.accountId);
        }
    }
}
