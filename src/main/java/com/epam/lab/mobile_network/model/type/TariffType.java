package com.epam.lab.mobile_network.model.type;

public enum TariffType {
    monthly,
    custom
}
