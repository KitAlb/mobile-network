package com.epam.lab.mobile_network.services;

import com.epam.lab.mobile_network.connection.ConnectionManager;
import com.epam.lab.mobile_network.dao.AccountDAO;
import com.epam.lab.mobile_network.dao.CallDAO;
import com.epam.lab.mobile_network.dao.TariffDAO;
import com.epam.lab.mobile_network.dao.implementation.AccountDaoImpl;
import com.epam.lab.mobile_network.dao.implementation.CallDaoImpl;
import com.epam.lab.mobile_network.dao.implementation.TariffDaoImpl;
import com.epam.lab.mobile_network.mappers.AccountMapper;
import com.epam.lab.mobile_network.model.Account;
import com.epam.lab.mobile_network.model.Call;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.SQLException;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static com.epam.lab.mobile_network.utils.Messages.BAD_UUID_MESSAGE;

public class CallService {
    private static Logger logger = LogManager.getLogger("ConsoleLogger");
    private static final int ERROR_STATUS = 0;

    private CallDAO callDao = new CallDaoImpl();
    private AccountDAO accountDao = new AccountDaoImpl();
    private TariffDAO tariffDao = new TariffDaoImpl();

    public Optional<List<Call>> findByReceiverId(String receiverIdString) {
        UUID id;
        try {
            id = UUID.fromString(receiverIdString);
        } catch (Exception ex) {
            logger.error(BAD_UUID_MESSAGE);
            return Optional.empty();
        }

        return callDao.findByReceiverId(id);
    }

    public Optional<List<Call>> findBySenderId(String senderIdString) {
        UUID id;
        try {
            id = UUID.fromString(senderIdString);
        } catch (Exception ex) {
            logger.error(BAD_UUID_MESSAGE);
            return Optional.empty();
        }

        return callDao.findBySenderId(id);
    }

    public int doCall(String senderIdString, String receiverIdString) {
        UUID senderId;
        UUID receiverId;
        try {
            senderId = UUID.fromString(senderIdString);
            receiverId = UUID.fromString(receiverIdString);
        } catch (Exception ex) {
            logger.error(BAD_UUID_MESSAGE);
            return ERROR_STATUS;
        }

        return accountDao.findByUserId(senderId).map(account -> {
            if (account.getFreeCall() > 0) {
                return decreaseFreeCalls(senderId, receiverId, account);
            } else {
                return chargeForCall(senderId, receiverId, account);
            }
        }).orElseGet(() -> {
            logger.error("Can't find user account");
            return ERROR_STATUS;
        });
    }

    private int decreaseFreeCalls(UUID senderId, UUID receiverId, Account account) {
        return ConnectionManager.getConnection().map(connection -> {
            int count = 0;
            try {
                try {
                    connection.setAutoCommit(false);
                    count += accountDao.update(
                            AccountMapper.updateFreebies(
                                    account,
                                    account.getFreeSms(),
                                    account.getFreeCall() - 1
                            )
                    );
                    count += callDao.create(Call.builder()
                            .senderId(senderId)
                            .receiverId(receiverId)
                            .build());
                    connection.commit();
                } catch (Exception ex) {
                    logger.error("Transaction exception: {}", ex.getMessage());
                    logger.error("Transaction is being rolled back");
                    connection.rollback();
                } finally {
                    connection.setAutoCommit(true);
                }
            } catch (SQLException ex) {
                logger.error("Exception when try to rollback transaction: {}", ex.getErrorCode());
            }

            return count;
        }).get();
    }

    private int chargeForCall(UUID senderId, UUID receiverId, Account account) {
        return tariffDao.findByUserId(senderId).map(tariff -> {
            if (account.getMoney() < tariff.getCallCost()) {
                logger.error("User doesn't have enough money for call");
                return ERROR_STATUS;
            }

            return chargeForCall(senderId, receiverId, account, tariff.getCallCost());
        }).orElseGet(() -> {
            logger.error("Can't find user's tariff");
            return ERROR_STATUS;
        });
    }

    private int chargeForCall(UUID senderId, UUID receiverId, Account account, Integer money) {
        return ConnectionManager.getConnection().map(connection -> {
            int count = 0;
            try {
                try {
                    connection.setAutoCommit(false);
                    count += accountDao.update(
                            AccountMapper.updateMoney(account, account.getMoney() - money)
                    );
                    count += callDao.create(Call.builder()
                            .senderId(senderId)
                            .receiverId(receiverId)
                            .build());
                    connection.commit();
                } catch (Exception ex) {
                    logger.error("Transaction exception: {}", ex.getMessage());
                    logger.error("Transaction is being rolled back");
                    connection.rollback();
                } finally {
                    connection.setAutoCommit(true);
                }
            } catch (SQLException ex) {
                logger.error("Exception when try to rollback transaction: {}", ex.getErrorCode());
            }

            return count;
        }).get();
    }

    public Optional<List<Call>> findAll() {
        return callDao.findAll();
    }
}
