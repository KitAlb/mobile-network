package com.epam.lab.mobile_network.services;

import com.epam.lab.mobile_network.dao.NotificationDAO;
import com.epam.lab.mobile_network.dao.UserDAO;
import com.epam.lab.mobile_network.dao.implementation.NotificationDaoImpl;
import com.epam.lab.mobile_network.dao.implementation.UserDaoImpl;
import com.epam.lab.mobile_network.model.Notification;
import com.epam.lab.mobile_network.model.User;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

import static com.epam.lab.mobile_network.utils.Constants.DATETIME_PATTERN;
import static com.epam.lab.mobile_network.utils.Messages.BAD_UUID_MESSAGE;

public class NotificationService {
    private static Logger logger = LogManager.getLogger("ConsoleLogger");
    private static final int VALID_PARAMS_COUNT = 3;
    private static final int ERROR_STATUS = 0;
    private static final int VALID_STATUS = 1;
    private static final String USER_ID_PARAM = "user_id";
    private static final String TRIGGER_DATE_PARAM = "trigger_date";
    private static final String TEXT_PARAM = "notification_text";

    private NotificationDAO notificationDao = new NotificationDaoImpl();
    private UserDAO userDao = new UserDaoImpl();
    private DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATETIME_PATTERN);

    public Optional<List<Notification>> findAll() {
        return notificationDao.findAll();
    }

    public Optional<Notification> findById(UUID id) {
        return notificationDao.findById(id);
    }

    public Optional<List<Notification>> findByUserId(UUID userId) {
        return notificationDao.findAllByUserId(userId);
    }

    public Optional<List<Notification>> findUpToDate(LocalDateTime localDateTime) {
        return notificationDao.findAllByDate(localDateTime);
    }

    public int createForAll(Map<String, String> params) {
        Optional<List<User>> users = userDao.findAll();
        if (users.isEmpty()) {
            logger.error("Cannot read users from database");
            return ERROR_STATUS;
        }

        if (CollectionUtils.isEmpty(users.get())) {
            logger.error("No users in database");
            return ERROR_STATUS;
        }

        AtomicInteger count = new AtomicInteger();
        users.get().forEach(user -> {
            Map<String, String> newParams = new HashMap<>();
            newParams.put(USER_ID_PARAM, user.getId().toString());
            params.forEach(newParams::put);
            count.addAndGet(create(newParams));
        });

        return count.get();
    }

    public int create(Map<String, String> params) {
        UUID userId = UUID.fromString(params.get(USER_ID_PARAM));
        if (isUserInvalid(userId) || isParamsInvalid(params)) {
            return ERROR_STATUS;
        }

        return create(Notification.builder()
                .userId(userId)
                .triggerDate(LocalDateTime.parse(params.get(TRIGGER_DATE_PARAM), formatter))
                .notificationText(params.get(TEXT_PARAM))
                .build()
        );
    }

    private int create(Notification entity) {
        return notificationDao.create(entity);
    }

    public int update(String idString, Map<String, String> params) {
        UUID id;
        try {
            id = UUID.fromString(idString);
        } catch (Exception ex) {
            logger.error(BAD_UUID_MESSAGE);
            return ERROR_STATUS;
        }

        UUID userId = UUID.fromString(params.get(USER_ID_PARAM));
        if(isUserInvalid(userId) || isParamsInvalid(params)) {
            return ERROR_STATUS;
        }

        return update(Notification.builder()
                .id(id)
                .userId(userId)
                .triggerDate(LocalDateTime.parse(params.get(TRIGGER_DATE_PARAM), formatter))
                .notificationText(params.get(TEXT_PARAM))
                .build()
        );
    }

    private int update(Notification entity) {
        return notificationDao.update(entity);
    }

    public int delete(String idString) {
        UUID id;
        try {
            id = UUID.fromString(idString);
        } catch (Exception ex) {
            logger.error(BAD_UUID_MESSAGE);
            return ERROR_STATUS;
        }

        return notificationDao.delete(id);
    }

    public List<String> getFields() {
        return List.of(
                USER_ID_PARAM, TRIGGER_DATE_PARAM, TEXT_PARAM
        );
    }

    public List<String> getCommonFields() {
        return List.of(TRIGGER_DATE_PARAM, TEXT_PARAM);
    }

    private boolean isParamsInvalid(Map<String, String> params) {
        if (params.size() != VALID_PARAMS_COUNT) {
            logger.error("Wrong parameters count");
            return true;
        }

        try {
            UUID.fromString(params.get(USER_ID_PARAM));
        } catch (Exception ex) {
            logger.error("User id is not valid UUID");
            return true;
        }

        try {
            LocalDateTime.parse(params.get(TRIGGER_DATE_PARAM), formatter);
        } catch (Exception ex) {
            logger.error("Trigger date and time should be in format '{}'", DATETIME_PATTERN);
            return true;
        }

        if (StringUtils.isBlank(params.get(TEXT_PARAM))) {
            logger.error("Notification text name cannot be empty");
            return true;
        }

        return false;
    }

    private boolean isUserInvalid(UUID userId) {
        if(userDao.findById(userId).isEmpty()) {
            logger.error("User with id '{}' doesn't exist", userId.toString());
            return true;
        }

        return false;
    }
}
