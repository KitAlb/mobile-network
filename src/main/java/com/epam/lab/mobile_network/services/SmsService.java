package com.epam.lab.mobile_network.services;

import com.epam.lab.mobile_network.connection.ConnectionManager;
import com.epam.lab.mobile_network.dao.AccountDAO;
import com.epam.lab.mobile_network.dao.SmsDAO;
import com.epam.lab.mobile_network.dao.TariffDAO;
import com.epam.lab.mobile_network.dao.implementation.AccountDaoImpl;
import com.epam.lab.mobile_network.dao.implementation.SmsDaoImpl;
import com.epam.lab.mobile_network.dao.implementation.TariffDaoImpl;
import com.epam.lab.mobile_network.mappers.AccountMapper;
import com.epam.lab.mobile_network.model.Account;
import com.epam.lab.mobile_network.model.Sms;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.SQLException;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static com.epam.lab.mobile_network.utils.Messages.BAD_UUID_MESSAGE;

public class SmsService {
    private static Logger logger = LogManager.getLogger("ConsoleLogger");
    private static final int ERROR_STATUS = 0;

    private SmsDAO smsDao = new SmsDaoImpl();
    private AccountDAO accountDao = new AccountDaoImpl();
    private TariffDAO tariffDao = new TariffDaoImpl();

    public Optional<List<Sms>> findByReceiverId(String receiverIdString) {
        UUID id;
        try {
            id = UUID.fromString(receiverIdString);
        } catch (Exception ex) {
            logger.error(BAD_UUID_MESSAGE);
            return Optional.empty();
        }

        return smsDao.findByReceiverId(id);
    }

    public Optional<List<Sms>> findBySenderId(String senderIdString) {
        UUID id;
        try {
            id = UUID.fromString(senderIdString);
        } catch (Exception ex) {
            logger.error(BAD_UUID_MESSAGE);
            return Optional.empty();
        }

        return smsDao.findBySenderId(id);
    }

    // TODO: update to return messages sorted by time for both users
    public Optional<List<Sms>> findBySenderAndReceiverId(String senderIdString, String receiverIdString) {
        UUID senderId;
        UUID receiverId;
        try {
            senderId = UUID.fromString(senderIdString);
            receiverId = UUID.fromString(receiverIdString);
        } catch (Exception ex) {
            logger.error(BAD_UUID_MESSAGE);
            return Optional.empty();
        }

        return smsDao.findBySenderAndReceiverId(senderId, receiverId);
    }

    // TODO: do the same using sender and receiver phone numbers
    public int sendSms(String senderIdString, String receiverIdString, String text) {
        UUID senderId;
        UUID receiverId;
        try {
            senderId = UUID.fromString(senderIdString);
            receiverId = UUID.fromString(receiverIdString);
        } catch (Exception ex) {
            logger.error(BAD_UUID_MESSAGE);
            return ERROR_STATUS;
        }

        if (StringUtils.isBlank(text)) {
            logger.error("Sms text cannot be empty");
            return ERROR_STATUS;
        }

        return accountDao.findByUserId(senderId).map(account -> {
            if (account.getFreeSms() > 0) {
                return decreaseFreeSms(senderId, receiverId, text, account);
            } else {
                return chargeForSms(senderId, receiverId, text, account);
            }
        }).orElseGet(() -> {
            logger.error("Can't find user account");
            return ERROR_STATUS;
        });
    }

    private int decreaseFreeSms(UUID senderId, UUID receiverId, String text, Account account) {
        return ConnectionManager.getConnection().map(connection -> {
            int count = 0;
            try {
                try {
                    connection.setAutoCommit(false);
                    count += accountDao.update(
                            AccountMapper.updateFreebies(
                                    account,
                                    account.getFreeSms() - 1,
                                    account.getFreeCall()
                            )
                    );
                    count += smsDao.create(Sms.builder()
                            .senderId(senderId)
                            .receiverId(receiverId)
                            .smsText(text)
                            .build());
                    connection.commit();
                } catch (Exception ex) {
                    logger.error("Transaction exception: {}", ex.getMessage());
                    logger.error("Transaction is being rolled back");
                    connection.rollback();
                } finally {
                    connection.setAutoCommit(true);
                }
            } catch (SQLException ex) {
                logger.error("Exception when try to rollback transaction: {}", ex.getErrorCode());
            }

            return count;
        }).get();
    }

    private int chargeForSms(UUID senderId, UUID receiverId, String text, Account account) {
        return tariffDao.findByUserId(senderId).map(tariff -> {
            if (account.getMoney() < tariff.getSmsCost()) {
                logger.error("User doesn't have enough money for sending sms");
                return ERROR_STATUS;
            }

            return chargeForSms(senderId, receiverId, text, account, tariff.getSmsCost());
        }).orElseGet(() -> {
            logger.error("Can't find user's tariff");
            return ERROR_STATUS;
        });
    }

    private int chargeForSms(UUID senderId, UUID receiverId, String text, Account account, Integer money) {
        return ConnectionManager.getConnection().map(connection -> {
            int count = 0;
            try {
                try {
                    connection.setAutoCommit(false);
                    count += accountDao.update(
                            AccountMapper.updateMoney(account, account.getMoney() - money)
                    );
                    count += smsDao.create(Sms.builder()
                            .senderId(senderId)
                            .receiverId(receiverId)
                            .smsText(text)
                            .build());
                    connection.commit();
                } catch (Exception ex) {
                    logger.error("Transaction exception: {}", ex.getMessage());
                    logger.error("Transaction is being rolled back");
                    connection.rollback();
                } finally {
                    connection.setAutoCommit(true);
                }
            } catch (SQLException ex) {
                logger.error("Exception when try to rollback transaction: {}", ex.getErrorCode());
            }

            return count;
        }).get();
    }

    public Optional<List<Sms>> findAll() {
        return smsDao.findAll();
    }
}
