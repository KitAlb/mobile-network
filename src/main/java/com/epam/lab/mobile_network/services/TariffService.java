package com.epam.lab.mobile_network.services;

import com.epam.lab.mobile_network.dao.TariffDAO;
import com.epam.lab.mobile_network.dao.UserDAO;
import com.epam.lab.mobile_network.dao.implementation.TariffDaoImpl;
import com.epam.lab.mobile_network.dao.implementation.UserDaoImpl;
import com.epam.lab.mobile_network.model.Tariff;
import com.epam.lab.mobile_network.model.type.TariffType;
import org.apache.commons.lang3.EnumUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import static com.epam.lab.mobile_network.utils.Constants.BASE_TARIFF_ID;
import static com.epam.lab.mobile_network.utils.Messages.BAD_UUID_MESSAGE;

public class TariffService {
    private static Logger logger = LogManager.getLogger("ConsoleLogger");
    private static final int VALID_PARAMS_COUNT = 6;
    private static final int ERROR_STATUS = 0;
    private static final int VALID_STATUS = 1;
    private static final String NAME_PARAM = "name";
    private static final String TYPE_PARAM = "type";
    private static final String SMS_COST_PARAM = "sms_cost";
    private static final String CALL_COST_PARAM = "call_cost";
    private static final String FREE_SMS_PARAM = "free_sms_count";
    private static final String FREE_CALL_PARAM = "free_call_count";
    private static final List<String> fields = List.of(
            NAME_PARAM, TYPE_PARAM, SMS_COST_PARAM, CALL_COST_PARAM, FREE_SMS_PARAM, FREE_CALL_PARAM
    );

    private TariffDAO tariffDao = new TariffDaoImpl();
    private UserDAO userDAO = new UserDaoImpl();

    public Optional<List<Tariff>> findAll() {
        return tariffDao.findAll();
    }

    public Optional<Tariff> findById(UUID id) {
        return tariffDao.findById(id);
    }

    public Optional<List<Tariff>> findByName(String name) {
        return tariffDao.findByName(name);
    }

    public Optional<List<Tariff>> findByType(TariffType type) {
        return tariffDao.findByType(type);
    }

    public int create(Map<String, String> params) {
        if (isParamsInvalid(params)) {
            return ERROR_STATUS;
        }

        return create(Tariff.builder()
                .name(params.get(NAME_PARAM))
                .tariffType(TariffType.valueOf(params.get(TYPE_PARAM)))
                .smsCost(NumberUtils.toInt(params.get(SMS_COST_PARAM)))
                .callCost(NumberUtils.toInt(params.get(CALL_COST_PARAM)))
                .freeSmsCount(NumberUtils.toInt(params.get(FREE_SMS_PARAM)))
                .freeCallCount(NumberUtils.toInt(params.get(FREE_CALL_PARAM)))
                .build()
        );
    }

    public int create(Tariff entity) {
        return tariffDao.create(entity);
    }

    public int update(String idString, Map<String, String> params) {
        UUID id;
        try {
            id = UUID.fromString(idString);
        } catch (Exception ex) {
            logger.error(BAD_UUID_MESSAGE);
            return ERROR_STATUS;
        }

        Optional<Tariff> tariff = tariffDao.findById(id);
        if (tariff.isEmpty()) {
            logger.error("Tariff wit id '{}' doesn't exists", id);
            return ERROR_STATUS;
        }

        if (isParamsInvalid(params)) {
            return ERROR_STATUS;
        }

        return update(Tariff.builder()
                .id(id)
                .name(params.get(NAME_PARAM))
                .tariffType(TariffType.valueOf(params.get(TYPE_PARAM)))
                .smsCost(NumberUtils.toInt(params.get(SMS_COST_PARAM)))
                .callCost(NumberUtils.toInt(params.get(CALL_COST_PARAM)))
                .freeSmsCount(NumberUtils.toInt(params.get(FREE_SMS_PARAM)))
                .freeCallCount(NumberUtils.toInt(params.get(FREE_CALL_PARAM)))
                .build()
        );
    }

    public int update(Tariff entity) {
        return tariffDao.update(entity);
    }

    public int delete(String idString) {
        UUID id;
        try {
            id = UUID.fromString(idString);
        } catch (Exception ex) {
            logger.error(BAD_UUID_MESSAGE);
            return ERROR_STATUS;
        }

        // Do not allow to delete base tariff
        // TODO: move BASE_TARIFF_ID to config
        if (id == BASE_TARIFF_ID) {
            logger.error("Cannot delete base tariff.");
            return ERROR_STATUS;
        }

        if (userDAO.findByTariffId(id).map(List::size).get() > 0) {
            logger.error("Cannot delete tariff for existing users. Please reassign users to different tariffs first.");
            return ERROR_STATUS;
        };

        return tariffDao.delete(id);
    }

    public List<String> getFields() {
        return fields;
    }

    // TODO: create separated generic validator service and custom validators for each data type
    private boolean isParamsInvalid(Map<String, String> params) {
        if (params.size() != VALID_PARAMS_COUNT) {
            logger.error("Wrong parameters count");
            return true;
        }

        if (StringUtils.isBlank(params.get(NAME_PARAM))) {
            logger.error("Tariff name cannot be empty");
            return true;
        }

        if (!EnumUtils.isValidEnum(TariffType.class, params.get(TYPE_PARAM))) {
            logger.error("Specified type is not valid");
            return true;
        }

        if (NumberUtils.toInt(params.get(SMS_COST_PARAM), -1) < 0) {
            logger.error("Sms cost should be positive numeric value");
            return true;
        }

        if (NumberUtils.toInt(params.get(CALL_COST_PARAM), -1) < 0) {
            logger.error("Call cost should be positive numeric value");
            return true;
        }

        if (NumberUtils.toInt(params.get(FREE_SMS_PARAM), -1) < 0) {
            logger.error("Free sms amount should be positive numeric value");
            return true;
        }

        if (NumberUtils.toInt(params.get(FREE_CALL_PARAM), -1) < 0) {
            logger.error("Free call amount should be positive numeric value");
            return true;
        }

        return false;
    }
}
