package com.epam.lab.mobile_network.services;

import com.epam.lab.mobile_network.connection.ConnectionManager;
import com.epam.lab.mobile_network.dao.*;
import com.epam.lab.mobile_network.dao.implementation.*;
import com.epam.lab.mobile_network.generators.PhoneGenerator;
import com.epam.lab.mobile_network.mappers.AccountMapper;
import com.epam.lab.mobile_network.mappers.UserMapper;
import com.epam.lab.mobile_network.model.Account;
import com.epam.lab.mobile_network.model.Tariff;
import com.epam.lab.mobile_network.model.User;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import static com.epam.lab.mobile_network.utils.Constants.BASE_TARIFF_ID;
import static com.epam.lab.mobile_network.utils.Messages.BAD_UUID_MESSAGE;

public class UserService {
    private static Logger logger = LogManager.getLogger("ConsoleLogger");
    private static final int ERROR_STATUS = 0;

    private static final String USER_FIRST_NAME_PARAM = "first_name";
    private static final String USER_LAST_NAME_PARAM = "last_name";

    private UserDAO userDao = new UserDaoImpl();
    private AccountDAO accountDao = new AccountDaoImpl();
    private TariffDAO tariffDao = new TariffDaoImpl();

    public Optional<List<User>> findByFirstName(String firstName) {
        return userDao.findByFirstName(firstName);
    }

    public Optional<List<User>> findByLastName(String lastName) {
        return userDao.findByLastName(lastName);
    }

    public Optional<List<User>> findByName(String firstName, String lastName) {
        return userDao.findByName(firstName, lastName);
    }

    public Optional<User> findByPhoneNumber(String phoneNumber) {
        return userDao.findByPhoneNumber(phoneNumber);
    }

    public Optional<List<User>> findByTariffId(String tariffId) {
        UUID id;
        try {
            id = UUID.fromString(tariffId);
            return userDao.findByTariffId(id);
        } catch (Exception ex) {
            logger.error(BAD_UUID_MESSAGE);
            return Optional.empty();
        }
    }

    public Optional<List<User>> findByTariffName(String tariffName) {
        return userDao.findByTariffName(tariffName);
    }

    public Optional<List<User>> findAll() {
        return userDao.findAll();
    }

    public Optional<User> findById(String idString) {
        UUID id;
        try {
            id = UUID.fromString(idString);
            return userDao.findById(id);
        } catch (Exception ex) {
            logger.error(BAD_UUID_MESSAGE);
            return Optional.empty();
        }
    }

    public int createNewUser(Map<String, String> params) {
        return ConnectionManager.getConnection().map(connection -> {
            if (isNewUserParamsInvalid(params)) {
                return ERROR_STATUS;
            }

            int userCount = 0;
            try {
                try {
                    connection.setAutoCommit(false);
                    // create new account for user
                    Account account = Account.builder().build();
                    int accountCount = accountDao.create(account);
                    if (accountCount == 0) {
                        throw new SQLException("Cannot create account");
                    }

                    User user = User.builder()
                            .accountId(account.getId())
                            .firstName(params.get(USER_FIRST_NAME_PARAM))
                            .lastName(params.get(USER_LAST_NAME_PARAM))
                            .phoneNumber(PhoneGenerator.generatePhoneNumber())
                            .tariffId(BASE_TARIFF_ID)
                            .build();
                    userCount = userDao.create(user);
                    connection.commit();
                } catch (Exception ex) {
                    logger.error("Transaction exception: {}", ex.getMessage());
                    logger.error("Transaction is being rolled back");
                    connection.rollback();
                } finally {
                    connection.setAutoCommit(true);
                }
            } catch (SQLException ex) {
                logger.error("Exception when try to rollback transaction: {}", ex.getErrorCode());
            }

            return userCount;
        }).get();
    }

    public int updateUserInfo(String idString, Map<String, String> params) {
        return findById(idString).map(user -> {
            if (isNewUserParamsInvalid(params)) {
                return ERROR_STATUS;
            }

            return update(UserMapper.updateInfo(user, params.get(USER_FIRST_NAME_PARAM), params.get(USER_LAST_NAME_PARAM)));
        }).orElseGet(() -> {
            logger.error("Can't find user with specified Id");
            return ERROR_STATUS;
        });
    }

    public int updateUserPhoneNumber(String idString, String phoneNumber) {
        return findById(idString).map(user -> {
            if (isPhoneNumberInvalid(phoneNumber)) {
                return ERROR_STATUS;
            }

            return update(UserMapper.updatePhoneNumber(user, phoneNumber));
        }).orElseGet(() -> {
            logger.error("Can't find user with specified Id");
            return ERROR_STATUS;
        });
    }

    public int deleteUser(String userIdString) {
        UUID id;
        try {
            id = UUID.fromString(userIdString);
        } catch (Exception ex) {
            logger.error(BAD_UUID_MESSAGE);
            return ERROR_STATUS;
        }

        return accountDao.findByUserId(id)
                .map(account -> deleteUser(id, account.getId()))
                .orElseGet(() -> {
                    logger.error("Can't find user account");
                    return ERROR_STATUS;
                });
    }

    private int deleteUser(UUID userId, UUID accountId) {
        return ConnectionManager.getConnection().map(connection -> {
            int count = 0;
            try {
                try {
                    connection.setAutoCommit(false);
                    count += userDao.delete(userId);
                    count += accountDao.delete(accountId);
                    connection.commit();
                } catch (Exception ex) {
                    logger.error("Transaction exception: {}", ex.getMessage());
                    logger.error("Transaction is being rolled back");
                    connection.rollback();
                } finally {
                    connection.setAutoCommit(true);
                }
            } catch (SQLException ex) {
                logger.error("Exception when try to rollback transaction: {}", ex.getErrorCode());
            }

            return count;
        }).get();
    }

    public int addMoneyToAccount(String userIdString, Integer amount) {
        UUID id;
        try {
            id = UUID.fromString(userIdString);
        } catch (Exception ex) {
            logger.error(BAD_UUID_MESSAGE);
            return ERROR_STATUS;
        }

        return accountDao.findByUserId(id)
                .map(account -> addMoneyToAccount(account, amount))
                .orElseGet(() -> {
                    logger.error("Can't find user account");
                    return ERROR_STATUS;
                });
    }

    private int addMoneyToAccount(Account account, Integer amount) {
        int newMoney = account.getMoney() + amount;
        if (newMoney < 0) {
            newMoney = 0;
        }

        return accountDao.update(AccountMapper.updateMoney(account, newMoney));
    }

    public int changeTariffId(String userIdString, String tariffIdString) {
        UUID userId;
        UUID tariffId;
        try {
            userId = UUID.fromString(userIdString);
            tariffId = UUID.fromString(tariffIdString);
        } catch (Exception ex) {
            logger.error(BAD_UUID_MESSAGE);
            return ERROR_STATUS;
        }

        return userDao.findById(userId)
                .map(user -> changeTariff(user, tariffId)).orElseGet(() -> {
                    logger.error("Can't find user with specified Id");
                    return ERROR_STATUS;
                });
    }

    private int changeTariff(User user, UUID tariffId) {
        return tariffDao.findById(tariffId)
                .map(tariff -> changeTariff(user, tariff))
                .orElseGet(() -> {
                    logger.error("Can't find tariff with specified Id");
                    return ERROR_STATUS;
                });
    }

    private int changeTariff(User user, Tariff tariff) {
        return accountDao.findById(user.getAccountId())
                .map(account -> changeTariff(user, tariff,account))
                .orElseGet(() -> {
                    logger.error("Can't find user account");
                    return ERROR_STATUS;
                });
    }

    private int changeTariff(User user, Tariff tariff, Account account) {
        return ConnectionManager.getConnection().map(connection -> {
            int count = 0;
            try {
                try {
                    connection.setAutoCommit(false);
                    count += userDao.update(UserMapper.updateTariff(user, tariff.getId()));
                    count += accountDao.update(
                            AccountMapper.updateFreebies(
                                    account,
                                    account.getFreeSms() + tariff.getFreeSmsCount(),
                                    account.getFreeCall() + tariff.getFreeCallCount()
                            )
                    );
                    connection.commit();
                } catch (Exception ex) {
                    logger.error("Transaction exception: {}", ex.getMessage());
                    logger.error("Transaction is being rolled back");
                    connection.rollback();
                } finally {
                    connection.setAutoCommit(true);
                }
            } catch (SQLException ex) {
                logger.error("Exception when try to rollback transaction: {}", ex.getErrorCode());
            }

            return count;
        }).get();
    }

    private int update(User entity) {
        return userDao.update(entity);
    }

    public List<String> getNewUserFields() {
        return List.of(USER_FIRST_NAME_PARAM, USER_LAST_NAME_PARAM);
    }

    private boolean isNewUserParamsInvalid(Map<String, String> params) {
        if (StringUtils.isBlank(params.get(USER_FIRST_NAME_PARAM))) {
            logger.error("First name cannot be empty");
            return true;
        }

        if (StringUtils.isBlank(params.get(USER_LAST_NAME_PARAM))) {
            logger.error("Last name cannot be empty");
            return true;
        }

        return false;
    }

    private boolean isPhoneNumberInvalid(String phoneNumber) {
        if (StringUtils.length(phoneNumber) != 7) {
            logger.error("Phone number should be 7 digits long number");
            return true;
        }

        if (!NumberUtils.isDigits(phoneNumber)) {
            logger.error("Phone number should be 7 digits long number");
            return true;
        }

        return false;
    }
}
