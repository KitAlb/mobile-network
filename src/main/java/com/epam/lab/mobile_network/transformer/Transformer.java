package com.epam.lab.mobile_network.transformer;

import com.epam.lab.mobile_network.model.annotation.Column;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

import static com.epam.lab.mobile_network.utils.Constants.DATETIME_PATTERN;

public class Transformer<T> {
    private final Class<T> clazz;

    public Transformer(Class<T> clazz) {
        this.clazz = clazz;
    }

    public T fromResultSetToEntity(ResultSet rs)
            throws SQLException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        Object builder = clazz.getMethod("builder").invoke(this.clazz);
        Map<String, Method> builderMethods = Arrays.stream(builder.getClass().getDeclaredMethods())
                .collect(Collectors.toMap(Method::getName, method -> method));
        for (Field field : clazz.getDeclaredFields()) {
            Column col = field.getAnnotation(Column.class);
            if (col != null) {
                field.setAccessible(true);
                String dbValue = rs.getString(col.name());
                if (dbValue == null) {
                    continue;
                }
                builderMethods.get(field.getName()).invoke(builder, getValueObject(field, dbValue));
            }
        }

        return (T) builderMethods.get("build").invoke(builder);
    }

    private Object getValueObject(Field field, String strValue) throws IllegalAccessException, InvocationTargetException, InstantiationException {
        try {
            Constructor constructor = field.getType().getConstructor(String.class);
            return  constructor.newInstance(strValue);
        } catch (NoSuchMethodException ignored) {
        }

        try {
            Method valueOfMethod = field.getType().getDeclaredMethod("valueOf", String.class);
            return valueOfMethod.invoke(field, strValue);
        } catch (NoSuchMethodException ignored) {
        }

        try {
            Method valueOfMethod = field.getType().getDeclaredMethod("fromString", String.class);
            return valueOfMethod.invoke(field, strValue);
        } catch (NoSuchMethodException ignored) {
        }

        try {
            if (field.getType() == LocalDateTime.class) {
                Method valueOfMethod = field.getType().getDeclaredMethod("parse", CharSequence.class, DateTimeFormatter.class);
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATETIME_PATTERN);
                return valueOfMethod.invoke(field, strValue, formatter);
            } else {
                Method valueOfMethod = field.getType().getDeclaredMethod("parse", String.class);
                return valueOfMethod.invoke(field, strValue);
            }
        } catch (NoSuchMethodException ignored) {
            System.out.println(ignored.getMessage());
        }

        return null;
    }
}
