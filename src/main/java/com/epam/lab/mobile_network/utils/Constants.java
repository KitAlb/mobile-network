package com.epam.lab.mobile_network.utils;

import java.util.UUID;

public class Constants {
    public static final String DB_CONNECTION = "dbConnection";
    public static final String DB_USER = "dbUser";
    public static final String DB_PASSWORD = "dbPassword";

    public static final String DATETIME_PATTERN = "yyyy-MM-dd HH:mm:ss";
    public static final UUID BASE_TARIFF_ID = UUID.fromString("9572510f-698f-4f82-a39f-c458dfff9e95");
}
