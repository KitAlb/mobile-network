DROP TABLE IF EXISTS `notification`;
DROP TABLE IF EXISTS `call`;
DROP TABLE IF EXISTS `sms`;
DROP TABLE IF EXISTS `user`;
DROP TABLE IF EXISTS `account`;
DROP TABLE IF EXISTS `tariff`;

CREATE TABLE `tariff` (
	`id` varchar(36) NOT NULL,
	`name` varchar(30) NOT NULL,
	`sms_cost` int NOT NULL,
	`call_cost` int NOT NULL,
	`free_sms_count` int NOT NULL,
	`free_call_count` int NOT NULL,
	`type` varchar(15) NOT NULL,
	PRIMARY KEY (`id`)
);

CREATE TABLE `account` (
	`id` varchar(36) NOT NULL,
	`money` int NOT NULL,
	`update_date` DATETIME NOT NULL,
    `free_sms` int NOT NULL,
    `free_call` int NOT NULL,
	PRIMARY KEY (`id`)
);

CREATE TABLE `user` (
	`id` varchar(36) NOT NULL,
	`first_name` varchar(30) NOT NULL,
	`last_name` varchar(30) NOT NULL,
	`tariff_id` varchar(36) NOT NULL,
	`phone_number` varchar(7) NOT NULL,
	`account_id` varchar(36) NOT NULL,
	PRIMARY KEY (`id`),
	CONSTRAINT `user_fk0` FOREIGN KEY (`tariff_id`) REFERENCES `tariff`(`id`),
    CONSTRAINT `user_fk1` FOREIGN KEY (`account_id`) REFERENCES `account`(`id`)
);

CREATE TABLE `sms` (
	`id` varchar(36) NOT NULL,
	`receiver_id` varchar(36) NOT NULL,
	`sender_id` varchar(36) NOT NULL,
	`datetime` DATETIME NOT NULL,
	`sms_text` varchar(200) NOT NULL,
	PRIMARY KEY (`id`),
    CONSTRAINT `sms_fk0` FOREIGN KEY (`receiver_id`) REFERENCES `user`(`id`),
    CONSTRAINT `sms_fk1` FOREIGN KEY (`sender_id`) REFERENCES `user`(`id`)
);

CREATE TABLE `call` (
	`id` varchar(36) NOT NULL,
	`receiver_id` varchar(36) NOT NULL,
	`sender_id` varchar(36) NOT NULL,
	`datetime` DATETIME NOT NULL,
	PRIMARY KEY (`id`),
    CONSTRAINT `ring_fk0` FOREIGN KEY (`receiver_id`) REFERENCES `user`(`id`),
    CONSTRAINT `ring_fk1` FOREIGN KEY (`sender_id`) REFERENCES `user`(`id`)
);

CREATE TABLE `notification` (
	`id` varchar(36) NOT NULL,
	`user_id` varchar(36) NOT NULL,
	`trigger_date` DATETIME NOT NULL,
	`notification_text` varchar(200) NOT NULL,
	PRIMARY KEY (`id`),
    CONSTRAINT `notifications_fk0` FOREIGN KEY (`user_id`) REFERENCES `user`(`id`)
);

INSERT INTO `tariff` VALUES
("9572510f-698f-4f82-a39f-c458dfff9e95","Base",10,10,0,0,"custom"),
("c5d1ce7a-a438-4321-accd-2dbe786ff2eb","Talker",10,3,0,200,"monthly"),
("713f48e6-c503-43cd-8e98-e94cb70e1ae4","Writer",3,10,200,0,"monthly"),
("e12e95ea-0191-4638-a5ca-cc9f5d2c5f43","Premium",3,3,300,200,"monthly"),
("a27dc148-2107-44ac-8973-20a740af8d79","Premium+",2,2,500,1000,"monthly");

INSERT INTO `account` VALUES
("2500a362-cab6-40ba-9420-96f4cad54ee2",2000,"2019-03-10 10:00:00",0,0),
("d030f26e-585b-4e34-8e78-1183b6020069",500,"2019-03-10 8:00:00",10,0),
("5858be1c-2cc4-462c-b2ce-860ca22d65ba",0,"2019-03-09 6:00:00",20,50),
("1bf82842-c6c0-4b6d-a55c-1aa84b0fb92c",100,"2019-02-10 00:00:00",5,10),
("054e520d-1e9e-444f-a5aa-77f5686677d3",50,"2019-03-05 00:00:00",20,20),
("f75c1807-b722-4c2e-b05c-c0599ac20f3a",5000,"2019-01-10 10:00:00",100,100),
("d4b7eee1-1a04-45d3-bcc5-31c5a8f6336c",200,"2019-02-05 11:34:00",50,20);

INSERT INTO `user` VALUES
("fb149482-500c-4f1a-a3a2-1ad637a6aa6b","Steve", "Jobs","9572510f-698f-4f82-a39f-c458dfff9e95","1111111","2500a362-cab6-40ba-9420-96f4cad54ee2"),
("1be52005-74e4-4558-a0a6-3839815c7b97","Michael","Jackson","c5d1ce7a-a438-4321-accd-2dbe786ff2eb","2222222","d030f26e-585b-4e34-8e78-1183b6020069"),
("aa5d53ea-d695-44df-83b6-d1ce00651a03","Danilo","Smirnov","713f48e6-c503-43cd-8e98-e94cb70e1ae4","3333333","5858be1c-2cc4-462c-b2ce-860ca22d65ba"),
("50b927b3-d127-40b0-92d8-b6c0c1aad2f2","Samuel","Jackson","e12e95ea-0191-4638-a5ca-cc9f5d2c5f43","7777777","1bf82842-c6c0-4b6d-a55c-1aa84b0fb92c"),
("0728da89-eb86-4200-a53c-ba9e042e0a27","Bobba", "Fett","a27dc148-2107-44ac-8973-20a740af8d79","4444444","054e520d-1e9e-444f-a5aa-77f5686677d3"),
("12b5dd2d-0200-4968-a14e-005355d87b4f","Han", "Solo","c5d1ce7a-a438-4321-accd-2dbe786ff2eb","5555555","f75c1807-b722-4c2e-b05c-c0599ac20f3a"),
("34c6de89-6bd8-4772-9cd9-4e3d16ebd3df","Luke","Starkiller","9572510f-698f-4f82-a39f-c458dfff9e95","6666666","d4b7eee1-1a04-45d3-bcc5-31c5a8f6336c");
