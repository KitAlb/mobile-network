package com.epam.lab.mobile_network.generators;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

class PhoneGeneratorTest {

    @Test
    @DisplayName("GIVEN valid input " +
            "WHEN generatePhoneNumber " +
            "THEN return valid object")
    void generatePhoneNumberValid() {
        // GIVEN
        int expectedSize = 7;

        // WHEN
        String actualResponse = PhoneGenerator.generatePhoneNumber();

        // THEN
        assertThat(actualResponse).containsOnlyDigits();
        assertThat(actualResponse).hasSize(expectedSize);
    }

}
