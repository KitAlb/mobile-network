package com.epam.lab.mobile_network.mappers;

import com.epam.lab.mobile_network.model.Account;

import java.time.LocalDateTime;
import java.util.UUID;

class AccountMapperFixture {

    private static final UUID ACCOUNT_ID = UUID.fromString("84b62d67-f1dc-4590-b2db-8023f3a80b80");
    private static final int MONEY = 100;
    private static final LocalDateTime UPDATE_DATE_TIME = LocalDateTime.now();
    private static final int ACCOUNT_FREE_SMS = 1;
    private static final int ACCOUNT_FREE_CALLS = 2;

    static final Account NEW_ACCOUNT =  Account.builder()
            .id(ACCOUNT_ID)
            .money(MONEY)
            .freeSms(ACCOUNT_FREE_SMS)
            .freeCall(ACCOUNT_FREE_CALLS)
            .updateDateTime(UPDATE_DATE_TIME)
            .build();

    static final int SMS_COUNT = 10;
    static final int CALLS_COUNT = 5;
    static final int MONEY_COUNT = 200;

    static final Account UPDATED_ACCOUNT = Account.builder()
            .id(ACCOUNT_ID)
            .money(MONEY)
            .updateDateTime(UPDATE_DATE_TIME)
            .freeSms(SMS_COUNT)
            .freeCall(CALLS_COUNT)
            .build();

    static final Account ACCOUNT_MONEY_UPDATED =  Account.builder()
            .id(ACCOUNT_ID)
            .money(MONEY_COUNT)
            .freeSms(ACCOUNT_FREE_SMS)
            .freeCall(ACCOUNT_FREE_CALLS)
            .updateDateTime(UPDATE_DATE_TIME)
            .build();
}
