package com.epam.lab.mobile_network.mappers;

import com.epam.lab.mobile_network.model.Account;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static com.epam.lab.mobile_network.mappers.AccountMapperFixture.*;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

class AccountMapperTest {

    @Test
    @DisplayName("GIVEN valid input " +
            "WHEN updateFreebies " +
            "THEN return valid object")
    void updateFreebiesValid() {
        // WHEN
        Account actualResponse = AccountMapper.updateFreebies(NEW_ACCOUNT, SMS_COUNT, CALLS_COUNT);

        // THEN
        assertThat(actualResponse).isEqualToComparingFieldByField(UPDATED_ACCOUNT);
    }

    @Test
    @DisplayName("GIVEN valid input " +
            "WHEN updateMoney " +
            "THEN return valid object")
    void updateMoneyValid(){
        // WHEN
        Account actualResponse = AccountMapper.updateMoney(NEW_ACCOUNT, MONEY_COUNT);

        // THEN
        assertThat(actualResponse).isEqualToComparingFieldByField(ACCOUNT_MONEY_UPDATED);
    }

}
