package com.epam.lab.mobile_network.mappers;

import com.epam.lab.mobile_network.model.User;

import java.util.UUID;

class UserMapperFixture {

    private static final UUID ID = UUID.fromString("3b908015-a9d0-4a77-bc8c-67b2a9866ef4");
    private static final UUID ACCOUNT_ID = UUID.fromString("6887e6cc-8f4a-4fe8-91c0-0fb165a092ae");
    private static final UUID TARIFF_ID = UUID.fromString("377b0c72-4396-486f-b4a5-4ad304309027");
    private static final String PHONE_NUMBER = "1232434354";
    private static final String FIRST_NAME = "Benedict";
    private static final String LAST_NAME = "Cumberbatch";

    static final User USER = User.builder()
            .id(ID)
            .accountId(ACCOUNT_ID)
            .phoneNumber(PHONE_NUMBER)
            .tariffId(TARIFF_ID)
            .lastName(LAST_NAME)
            .firstName(FIRST_NAME)
            .build();

    static final String NEW_FIRST_NAME = "Keanu";
    static final String NEW_LAST_NAME = "Reeves";
    static final String NEW_PHONE_NUMBER = "999999999";
    static final UUID NEW_TARIFF_ID = UUID.fromString("f122792b-9ecc-4b56-85a6-5a9c98d24ae5");

    static final User USER_NEW_NAMES = User.builder()
            .id(ID)
            .accountId(ACCOUNT_ID)
            .phoneNumber(PHONE_NUMBER)
            .tariffId(TARIFF_ID)
            .lastName(NEW_LAST_NAME)
            .firstName(NEW_FIRST_NAME)
            .build();
    static final User USER_NEW_PHONE_NUMBER = User.builder()
            .id(ID)
            .accountId(ACCOUNT_ID)
            .phoneNumber(NEW_PHONE_NUMBER)
            .tariffId(TARIFF_ID)
            .lastName(LAST_NAME)
            .firstName(FIRST_NAME)
            .build();
    static final User USER_NEW_TARIFF = User.builder()
            .id(ID)
            .accountId(ACCOUNT_ID)
            .phoneNumber(PHONE_NUMBER)
            .tariffId(NEW_TARIFF_ID)
            .lastName(LAST_NAME)
            .firstName(FIRST_NAME)
            .build();

}
