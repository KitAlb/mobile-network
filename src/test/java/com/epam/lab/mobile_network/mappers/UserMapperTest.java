package com.epam.lab.mobile_network.mappers;

import com.epam.lab.mobile_network.model.User;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static com.epam.lab.mobile_network.mappers.UserMapperFixture.*;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

class UserMapperTest {

    @Test
    @DisplayName("GIVEN valid parameters " +
            "WHEN updateInfo " +
            "THEN return valid object")
    void updateInfoValid() {
        // WHEN
        User actualResponse = UserMapper.updateInfo(USER, NEW_FIRST_NAME, NEW_LAST_NAME);
        // THEN
        assertThat(actualResponse).isEqualToComparingFieldByField(USER_NEW_NAMES);
    }

    @Test
    @DisplayName("GIVEN valid parameters " +
            "WHEN updatePhoneNumber " +
            "THEN return valid object")
    void updatePhoneNumberValid() {
        // WHEN
        User actualResponse = UserMapper.updatePhoneNumber(USER, NEW_PHONE_NUMBER);
        // THEN
        assertThat(actualResponse).isEqualToComparingFieldByField(USER_NEW_PHONE_NUMBER);
    }

    @Test
    @DisplayName("GIVEN valid parameters " +
            "WHEN updateTariff " +
            "THEN return valid object")
    void updateTariffValid() {
        // WHEN
        User actualResponse = UserMapper.updateTariff(USER, NEW_TARIFF_ID);
        // THEN
        assertThat(actualResponse).isEqualToComparingFieldByField(USER_NEW_TARIFF);
    }

}
