package com.epam.lab.mobile_network.services;

import com.epam.lab.mobile_network.model.User;

import java.util.List;
import java.util.UUID;

class UserServiceFixture {

    static final String TARIFF_ID_STR_VALID = "18510ada-f6b5-4d6f-acd9-2199e7761c33";
    static final String TARIFF_ID_STR_INVALID = "uuid";
    static final UUID TARIFF_ID = UUID.fromString(TARIFF_ID_STR_VALID);
    static final String USER_ID_STR_VALID = "90ebdd28-c88c-4c9f-a46c-063d50a35a51";
    static final String USER_ID_STR_INVALID = "userId";
    static final UUID USER_ID = UUID.fromString(USER_ID_STR_VALID);
    static final String TARIFF_NAME = "Super Economy";
    static final String FIRST_NAME = "Benedict";
    static final String LAST_NAME = "Cumberbatch";
    static final String PHONE_NUMBER = "9999999";

    static final User USER_BENEDICT_REEVES = User.builder()
            .firstName(FIRST_NAME)
            .lastName("Reeves")
            .tariffId(TARIFF_ID)
            .build();
    static final User USER_BENEDICT_CUMBERBATCH = User.builder()
            .id(USER_ID)
            .firstName(FIRST_NAME)
            .lastName(LAST_NAME)
            .phoneNumber(PHONE_NUMBER)
            .tariffId(TARIFF_ID)
            .build();
    static final User USER_KEANU_CUMBERBATCH = User.builder()
            .firstName("Keanu")
            .lastName(LAST_NAME)
            .build();
    static final User USER_KEANU_REEVES = User.builder()
            .firstName("Keanu")
            .lastName("Reeves")
            .build();

    static final List<User> BENEDICT_LIST = List.of(
            USER_BENEDICT_CUMBERBATCH,
            USER_BENEDICT_REEVES
    );
    static final List<User> CUMBERBATCH_LIST = List.of(
            USER_BENEDICT_CUMBERBATCH,
            USER_KEANU_CUMBERBATCH
    );
    static final List<User> BENEDICT_CUMBERBATCH_LIST = List.of(
            USER_BENEDICT_CUMBERBATCH
    );
    static final List<User> ALL_USERS_LIST = List.of(
            USER_BENEDICT_CUMBERBATCH,
            USER_BENEDICT_REEVES,
            USER_KEANU_CUMBERBATCH,
            USER_KEANU_REEVES
    );

}
