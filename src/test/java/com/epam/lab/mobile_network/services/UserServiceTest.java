package com.epam.lab.mobile_network.services;

import com.epam.lab.mobile_network.dao.AccountDAO;
import com.epam.lab.mobile_network.dao.TariffDAO;
import com.epam.lab.mobile_network.dao.UserDAO;
import com.epam.lab.mobile_network.model.User;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Optional;

import static com.epam.lab.mobile_network.services.UserServiceFixture.*;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class UserServiceTest {

    @Mock
    private UserDAO userDao;
    @Mock
    private AccountDAO accountDao;
    @Mock
    private TariffDAO tariffDao;
    @InjectMocks
    private UserService service = new UserService();

    @Test
    @DisplayName("GIVEN valid parameters " +
            "WHEN findByFirstName " +
            "THEN return valid response")
    void findByFirstNameValid() {
        // GIVEN
        given(userDao.findByFirstName(eq(FIRST_NAME)))
                .willReturn(Optional.of(BENEDICT_LIST));

        // WHEN
        Optional<List<User>> actualResponse = service.findByFirstName(FIRST_NAME);

        // THEN
        assertThat(actualResponse)
                .contains(BENEDICT_LIST);
        verify(userDao, only()).findByFirstName(FIRST_NAME);
        verifyNoInteractions(accountDao);
        verifyNoInteractions(tariffDao);
    }

    @Test
    @DisplayName("GIVEN valid parameters " +
            "WHEN findByFirstName " +
            "BUT DAO return empty response " +
            "THEN return valid response")
    void findByFirstNameEmpty() {
        // GIVEN
        given(userDao.findByFirstName(any(String.class)))
                .willReturn(Optional.of(List.of()));

        // WHEN
        Optional<List<User>> actualResponse = service.findByFirstName(FIRST_NAME);

        // THEN
        assertThat(actualResponse)
                .contains(List.of());
        verify(userDao, only()).findByFirstName(FIRST_NAME);
        verifyNoInteractions(accountDao);
        verifyNoInteractions(tariffDao);
    }

    @Test
    @DisplayName("GIVEN valid parameters " +
            "WHEN findByLastName " +
            "THEN return valid response")
    void findByLastNameValid() {
        // GIVEN
        given(userDao.findByLastName(eq(LAST_NAME)))
                .willReturn(Optional.of(CUMBERBATCH_LIST));

        // WHEN
        Optional<List<User>> actualResponse = service.findByLastName(LAST_NAME);

        // THEN
        assertThat(actualResponse)
                .contains(CUMBERBATCH_LIST);
        verify(userDao, only()).findByLastName(LAST_NAME);
        verifyNoInteractions(accountDao);
        verifyNoInteractions(tariffDao);
    }

    @Test
    @DisplayName("GIVEN valid parameters " +
            "WHEN findByName " +
            "THEN return valid response")
    void findByNameValid() {
        // GIVEN
        given(userDao.findByName(eq(FIRST_NAME), eq(LAST_NAME)))
                .willReturn(Optional.of(BENEDICT_CUMBERBATCH_LIST));

        // WHEN
        Optional<List<User>> actualResponse = service.findByName(FIRST_NAME, LAST_NAME);

        // THEN
        assertThat(actualResponse)
                .contains(BENEDICT_CUMBERBATCH_LIST);
        verify(userDao, only()).findByName(FIRST_NAME, LAST_NAME);
        verifyNoInteractions(accountDao);
        verifyNoInteractions(tariffDao);
    }

    @Test
    @DisplayName("GIVEN valid parameters " +
            "WHEN findByPhoneNumber " +
            "THEN return valid response")
    void findByPhoneNumberValid() {
        // GIVEN
        given(userDao.findByPhoneNumber(eq(PHONE_NUMBER)))
                .willReturn(Optional.of(USER_BENEDICT_CUMBERBATCH));

        // WHEN
        Optional<User> actualResponse = service.findByPhoneNumber(PHONE_NUMBER);

        // THEN
        assertThat(actualResponse)
                .contains(USER_BENEDICT_CUMBERBATCH);
        verify(userDao, only()).findByPhoneNumber(PHONE_NUMBER);
        verifyNoInteractions(accountDao);
        verifyNoInteractions(tariffDao);
    }

    @Test
    @DisplayName("GIVEN valid parameters " +
            "WHEN findByTariffId " +
            "THEN return valid response")
    void findByTariffIdValid() {
        // GIVEN
        given(userDao.findByTariffId(eq(TARIFF_ID)))
                .willReturn(Optional.of(BENEDICT_LIST));

        // WHEN
        Optional<List<User>> actualResponse = service.findByTariffId(TARIFF_ID_STR_VALID);

        // THEN
        assertThat(actualResponse)
                .contains(BENEDICT_LIST);
        verify(userDao, only()).findByTariffId(TARIFF_ID);
        verifyNoInteractions(accountDao);
        verifyNoInteractions(tariffDao);
    }

    @Test
    @DisplayName("GIVEN invalid tariff id string " +
            "WHEN findByTariffId " +
            "THEN return empty response")
    void findByTariffIdInvalid() {
        // GIVEN

        // WHEN
        Optional<List<User>> actualResponse = service.findByTariffId(TARIFF_ID_STR_INVALID);

        // THEN
        assertThat(actualResponse)
                .isEmpty();
        verifyNoInteractions(userDao);
        verifyNoInteractions(accountDao);
        verifyNoInteractions(tariffDao);
    }

    @Test
    @DisplayName("GIVEN valid parameters " +
            "WHEN findByTariffName " +
            "THEN return valid response")
    void findByTariffNameValid() {
        // GIVEN
        given(userDao.findByTariffName(eq(TARIFF_NAME)))
                .willReturn(Optional.of(BENEDICT_LIST));

        // WHEN
        Optional<List<User>> actualResponse = service.findByTariffName(TARIFF_NAME);

        // THEN
        assertThat(actualResponse)
                .contains(BENEDICT_LIST);
        verify(userDao, only()).findByTariffName(TARIFF_NAME);
        verifyNoInteractions(accountDao);
        verifyNoInteractions(tariffDao);
    }

    @Test
    @DisplayName("GIVEN valid parameters " +
            "WHEN findAll " +
            "THEN return valid response")
    void findAllValid() {
        // GIVEN
        given(userDao.findAll())
                .willReturn(Optional.of(ALL_USERS_LIST));

        // WHEN
        Optional<List<User>> actualResponse = service.findAll();

        // THEN
        assertThat(actualResponse)
                .contains(ALL_USERS_LIST);
        verify(userDao, only()).findAll();
        verifyNoInteractions(accountDao);
        verifyNoInteractions(tariffDao);
    }

    @Test
    @DisplayName("GIVEN valid parameters " +
            "WHEN findById " +
            "THEN return valid response")
    void findByIdValid() {
        // GIVEN
        given(userDao.findById(eq(USER_ID)))
                .willReturn(Optional.of(USER_BENEDICT_CUMBERBATCH));

        // WHEN
        Optional<User> actualResponse = service.findById(USER_ID_STR_VALID);

        // THEN
        assertThat(actualResponse)
                .contains(USER_BENEDICT_CUMBERBATCH);
        verify(userDao, only()).findById(USER_ID);
        verifyNoInteractions(accountDao);
        verifyNoInteractions(tariffDao);
    }

    @Test
    @DisplayName("GIVEN invalid user id string " +
            "WHEN findById " +
            "THEN return empty response")
    void findByIdInvalid() {
        // GIVEN

        // WHEN
        Optional<User> actualResponse = service.findById(USER_ID_STR_INVALID);

        // THEN
        assertThat(actualResponse)
                .isEmpty();
        verifyNoInteractions(userDao);
        verifyNoInteractions(accountDao);
        verifyNoInteractions(tariffDao);
    }

}
